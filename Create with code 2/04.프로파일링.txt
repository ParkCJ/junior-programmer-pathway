﻿[Junior programmer pathway - Profile code to identify issues]
1. 게임View의 Stats 탭을 눌러, FPS를 확인한다.
-> FPS가 좋지 않을 경우, 원인을 찾는다.

2. Windows/Analysis/Profiler 를 열어, ms budget을 확인한다.
1000 ms / 목표 frames per second = ms budget
예) 60 FPS의 경우, 16ms budget

3. 어떤 스크립트의 어떤 메소드에서 시간을 잡아먹고 있는지 확인한다.
4. 메소드 내의 큰 기능별로 아래와 같이 문구를 넣어, 프로파일링 샘플을 나눈다.
예)
Profiler.BeginSample("Moving"); // begin profiling
Move();
Profiler.EndSample(); // end profiling
5. Timeline -> Hierarchy로 변경후, 어떤 기능이 시간을 잡아먹고 있는지 확인한다.
6. 시간을 잡아먹고 있는 기능의 코드를 수정한다.
7. 원하는 FPS가 나올때까지 반복한다.