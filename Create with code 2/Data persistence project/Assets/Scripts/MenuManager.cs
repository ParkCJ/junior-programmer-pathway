using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEditor;

public class MenuManager : MonoBehaviour
{
    [SerializeField] Text bestScoreText;
    [SerializeField] InputField nameInput;
    [SerializeField] Button startBtn;
    [SerializeField] Button quitBtn;

    // Start is called before the first frame update
    void Start()
    {
        bestScoreText.text = $"BEST SCORE : {PersistenceManager.Instance.BestScorePlayerName} : {PersistenceManager.Instance.BestScore}";
    }

    public void StartGame()
    {
        PersistenceManager.Instance.PlayerName = nameInput.text;
        SceneManager.LoadScene(1);
    }

    public void QuitGame()
    {
#if UNITY_EDITOR
        EditorApplication.ExitPlaymode();
#else
        Application.Quit();
#endif
    }
}
