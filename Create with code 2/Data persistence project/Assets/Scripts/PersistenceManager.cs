using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

public class PersistenceManager : MonoBehaviour
{
    public static PersistenceManager Instance;
    public string PlayerName;
    public string BestScorePlayerName;
    public int BestScore;

    private void Awake()
    {
        if (PersistenceManager.Instance != null)
        {
            Destroy(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(gameObject);

        LoadData();
    }

    public void SaveData()
    {
        SaveDataModel data = new SaveDataModel();
        data.BestScorePlayerName = PlayerName;
        data.BestScore = BestScore;

        string json = JsonUtility.ToJson(data);

        File.WriteAllText(Application.persistentDataPath + "/savefile.json", json);
    }

    public void LoadData()
    {
        string path = Application.persistentDataPath + "/savefile.json";
        if (File.Exists(path))
        {
            string json = File.ReadAllText(path);
            SaveDataModel data = JsonUtility.FromJson<SaveDataModel>(json);

            BestScorePlayerName = data.BestScorePlayerName;
            BestScore = data.BestScore;
        }
    }

    [Serializable]
    class SaveDataModel
    {
        public string BestScorePlayerName;
        public int BestScore;
    }
}
