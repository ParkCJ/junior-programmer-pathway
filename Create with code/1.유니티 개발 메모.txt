﻿1.
start simple,
start with the basics,
and let yourself be motivated by that special idea
- the idea that will keep driving you forward to learn more.

간단한거
기본 기술로
너가 만들고 싶은 걸


3. 컴포넌트: 행동과 속성을 추가
1) Rigid body component: 물리가 적용되도록 만드는 것 (중력 등)
	오브젝트가 중력과 공기 밀도에 어떻게 상호작용 하는지를 조정한다.
	Mass: 무게 (킬로그램)
	Constraints: 물리 영향을 받아도, 특정 축으로는 움직여지지 않게, 특정 축으로는 회전되지 않게 고정할 수 있다.
		FreezePosition x, y, z
		FreezeRotation x, y, z
	centerOfMass: 무게 중심점 (Vector3) - 기본적으로 자동 계산됨. 수정을 하고 싶으면 (예: 차의 중심점 낮추기),
		Empty GameObject 생성 후 자식으로 붙인다음에, 이 Object의 Position으로 설정하면 GameView에서 쉽게 변경 가능.
	velocity: 속도 (Vector3) -> velocity.magnitude: 수치로 받을 때.
		속도를 km/h로 계산하기: velocity.magnitude * 3.6f
		속도를 마일/h로 계산하기: velocity.magnitude * 2.2369362912f

2) 머티리얼 (이미지 파일): 표면 특성을 추가. 피부라고 생각하자.
3) Collider: "물리가 적용되는 모양" 개념을 추가 (3D primitive는 기본적으로 built-in. 2D sprite는 직접 추가해야함)
	<https://docs.unity3d.com/Manual/CollidersOverview.html>
	오브젝트들이 서로 어떻게 상호작용 하는지를 조정한다.
	콜라이더가 없는건, 보이긴 하는데 만질수 없는 유령같은 거라고 생각하면 됨.
	피지컬 머티리얼: 마찰, 튕김등의 특성을 추가. 중력의 영향을 받는 물체에게만 영향을 줌.
		(https://docs.unity3d.com/kr/2020.3/Manual/class-PhysicMaterial.html)
		- Fiction Combine: 두 물체가 부딪혔을 때, 둘의 마찰력중 어느 마찰력을 적용할 것인가.
		- Bounce Combine: 두 물체가 부딪혔을 때, 둘의 Bounciness 중 어느 것을 적용할 것인가.
	WheelColider: 자동차 바퀴를 위해 만들어진 특별한 콜라이더
		IsGrounded: 다른 물체와 Collide 상태인지 여부를 반환
	Static collider: Collider가 붙어있고, RigidBody는 붙어있지 않은 경우. 바닥, 벽과 같이 움직임이 없는 물체를 만들때 쓰인다.
	Dynamic collider: Collider, RigidBody 모두 붙어있는 경우. 물리엔진에 따라 움직인다. 

* Rigid body vs Collider
The RigidBody 2D Component also gives a Sprite physical properties, so what is the difference?
RigidBody properties control how the GameObject interacts with gravity and air density.
For example, the RigidBody properties on the Circle Sprite make it fall, but when it hits another GameObject, it will pass through it.
The Collider Component adds additional properties that determine how objects interact with each other.
By adding a Collider Component to the Circle and a Sprite below it to represent the ground,
the Circle will stop its downward trajectory when it reaches the ground.

4) Collider, RigidBody & IsKinematic
움직이지 않는 벽 : Collider
움직이지 않는 감지 물체 : Collider(IsTrigger)
물리로 움직이는 벽 : Collider & Rigidbody
물리로 움직이는 감지 물체 : Collider(IsTrigger) & Rigidbody
transform으로 움직이는 벽 : Collider & Rigidbody(IsKinematic)
transform으로 움직이는 감지 물체 : Collider(IsTrigger) & Rigidbody(IsKinematic)
물리 이동만 한다면 : Rigidbody
transform 이동만 한다면 : Rigidbody(IsKinematic)

4. 프레임: 한장의 이미지
5. 스프라이트: 2D 신의 게임 오브젝트. 이미지이다. 속성을 통해 인터렉티브하게 움직인다.
6. Translate vs AddForce:
	Translate: 오브젝트를 드래그 하는 것 (특정시간에 특정 거리만큼. 물리엔진과 관련 없다.)
	AddForce: 미는 것 (물리가 적용된다. 물체의 무게, 미는 힘, 중력)
		AddForce(Vector3 <-- Vector3의 '거리' 가 멀수록, 더 큰 힘을 가한다.
		거리랑 상관없이 같은 힘을 적용하고 싶으면 (Vector3의 '방향' 개념만 적용), Vector3.normalized를 사용 (거리값을 -1 ~ 1 사이의 값으로 변경한 Vector3를 return 한다.)

	cf) AddTorque: 회전을 가하는 것 (AddForce와 마찬가지로 물리가 적용된다.)

7. GameObject를 찾는 방법
1) 태그를 사용하는 방법. ex) collision.gameObject.CompareTag (OnCollisionEnter 메소드)
2) GameObject.Find를 이용하는 방법 (신에 존재하는 GameObject를 찾는다.)
3) FindObjectsOfType를 이용하는 방법 (신에 존재하는 특정 스크립트등을 가진 GameObject 들을 찾는다.)
4) Public으로 변수 선언후, 드래그 해서 넣어서, 참조하는 방법

8. 애니메이션
1) 애니메이션을 적용한다는 의미
GameObject에 애니메이션을 적용한다는 것은, GameObject에 애니메이션이 실행되고 있다는 뜻. (Transform 값이 지속적으로 변경되고 있다는 뜻)
특정 조건이 발생하면 (점프 등) -> 애니메이터의 상태를 다른 상태로 변경해서 -> 다른 애니메이션이 실행되게 한다.
	시작시점에 동작할 애니메이션 설정하기: Default 스태이트로 설정한다. (시작하자마자 엔트리 스테이트에서, Default로 설정된 스테이트로 바로 이동한다.)
	게임중에 애니메이션 변경하기: 파라미터 값을 변경해, 다른 스태이트로 이동하는 조건을 만족시킨다.

9. 파티클 (ParticleSystem 컴포넌트 사용)
-사용법1: 몸에 붙여놓는 물풍선이라고 생각하자.
1) 실행버튼을 누르면 실행되고 끝난다.
2) GameObject에 차일드오브젝트로 추가해, 특정 위치에 위치시킨다.
3) 특정 조건 발생시 실행한다.
4) Play on awake가 체크되어 있으면, 플레이 시작되자마자 실행된다.
-사용법2: Play on awake를 체크한 후에, 특정 상황에서, 특정 위치에 Instantiate 한다.

10. 오디오
1) 메인카메라에 리스너가 붙어있다.
2) Audio source 컴포넌트를 이용한다. (배경음악의 경우, 카메라에 컴포넌트를 추가하자.)
	Audio source 컴포넌트: CD 플레이어와 같은 재생기라고 생각하자.
3) Audio source 컴포넌트의 AudioClip 속성에 "재생할 애" 를 집어넣어서 재생.
4) 사운드이펙트 (SFX): 재생이 필요할 시, AudioSource.PlayOneShot(오디오클립, 볼륨) 을 사용해 재생.
5) 게임오브젝트마다 Audio source컴포넌트를 추가해서 자신의 소리를 가지고 있게 할 수 있다. (Loop로 재생)
-> 3D Spacial 적용해서 거리 적용되도록
-> 파괴될때는, 파괴SFX를 재생

11. 텍스쳐 (옷 이라고 생각하자)
GameObject에 드래그앤드롭해서 설정한다.

12. 글로벌 스페이스, 로컬 스페이스
	글로벌 스페이스: Scene 의 세상을 기준으로 하는 절대 좌표축 (지도의 동서남북 마크라고 생각하자. 세상의 동서남북을 확인하고 싶을때 사용)
	로컬 스페이스: 나 자신을 기준으로 하는 좌표축 (내가 지금 어느축으로 회전해 있는가 확인가능)
	
13. 벡터: 두 Object 사이의 "거리와 방향"
1) A물체.position <----벡터---> B물체.position
2) A물체.position + 벡터 = B물체.position

14. Coroutine: 코드 실행을 몇초 이후로 지연시킨다. 타이머 만들 때 좋다.
ex) 아이템 먹고, 몇초간 효과 유지하고, 이후 사라지는 것

15. UI (User interface): 사용자가 실행중 읽고 상호반응 할 수 있는 "텍스트, 버튼, 디스플레이"
1) 게임을 Fully playable 하게 만든다. (Fully playable: 버그나 빠진 것 등 방해없이 처음부터 끝까지 쭉 플레이 할 수 있는 게임)
2) Anchor: 다른 크기의 화면에서, 같은 위치에 UI를 표시
3) Button의 Click 이벤트 처리
	- 인스펙터의 On Click (): 특정 GameObject를 여기에 끌어다 놓으면, 끌어다 놓은 애의 Public Method를 선택해 실행시킬 수 있다.
	- 스크립트의 AddListner: 자기 자신의 private 메소드를 실행시킬 수 있다.

16. GameManager 스크립트
1) 사람들이 많이 사용하는 이름으로, 유니티에서는 특별한 아이콘을 붙여준다.
2) 그 외 다른건 없다.

17. OnMouseDown
1) Object 자신에 클릭하는 경우, 실행됨. 자주 쓰여서, 유니티에서 미리 만들어놓은 메소드.
2) cf) OnMouseUp: 클릭 후 마우스를 땔 때 실행됨

19. SceneManager (UnityEngine.SceneManagement)
1) Scene 관련 라이브러리. Scene을 로드하거나, 현재 Scene을 가져오거나 등.

20. 프로젝트 최적화
1) private, public, protected, readonly, const, static, [SerializeField] (Unity Editor에서 편집할 수 있게 하는 기능) 를 적절히 사용
2) awake, start, fixedUpdate, update, lateUpdate
	awake: Object가 깨어날 때 실행. 실행 후 Object의 인스턴스가 만들어진다. start 전에 실행된다. 게임 시작전 object 를 설정할 때 사용하자. script가 disabled 되어있어도 실행됨.
	start: 첫번째 Update 가 실행되기 전에 실행된다. 게임 시작의 의미. script가 enabled 되어 있어야만 실행됨.
	fixedUpdate: 움직임, 물리 연산은 여기서 처리 (Update 가 호출되기 전에 호출된다.)
	lateUpdate: 카메라 포지션 계산은 여기서 처리 (Update 종류들의 모든 연산이 끝난후, 마지막에 호출된다.)
3) 안쓰는 빈 start, update 메소드 등을 코드에서 삭제
4) ObjectPooling 을 사용 (Instantiate, Destroy 대신에, 게임시작시 Object들을 몇십개 미리 만들어 넣어두고, Active, DeActive 하면서 사용한다. 대형게임에서 성능향상)
	<Prototype 2 코드 참조>
	GameManager에 List<GameObject>를 만들고, 시작시 GameObject 10개를 Instantiate해서 (Active = false) 넣어서 참조를 만들어둠 (화면에 10개가 존재하나 안보임. List<GameObject>로 찾을 수 있음)
	이 GameObject가 필요할 때, List<GameObject> 중에서 Active = false인 애를 찾아서 Active = true 하고, Position을 설정하고 사용함
	이 GameObject가 필요없을 때, Destroy 대신에 Active = false를 실행.
	즉, 화면에 안보이게 (InActive) Object 만들어두고, 필요할 때 Active 해서 쓰고, 필요없어지면 InActive 하는 방법!

21. 트러블슈팅
22. 프로젝트 배포하기
1) 빌드하기
	[Windows]
	파일/빌드 셋팅 -> Add Open Scenes 으로 신 추가
	Player Settings 조정 (예: Windowsed, Risizable 등)
	Build
	테스트 플레이

	[WebGL]
	파일/빌드 셋팅 -> Add Open Scenes 으로 신 추가
	Player Settings 조정 (예: Windowsed, Risizable 등)
	Build and play (실행하려면 웹서버가 필요. Build and play를 하면 임시로 유니티가 웹서버 위에 띄워줌)
	Build를 저장한 폴더를 zip으로 압축
	Unity Play 또는 itch.io 에 업로드

23. Entity Component System (ECS)
1) Data-Oriented Technology Stack (DOTS) - 기존의 Object-Oriented와 다르다.
2) Code is structured around key pieces of data (e.g. MoveSpeed, PlayerHealth) not Objects (예: enemy, Obstacle) and not components (예: MoveForward, DestroyOnCollision).
3) 사용이유: 이 방식은 성능과 최적화에 엄청난 향상이 있다.
4) ECS 인지 구분하는 방법
	- using Unity.Entities: Unity.Entities 네임스페이스 사용

24. Unity Junior Programmer 과정 종료
	현재 시점에서 나는 직업 중
		Junior Software Developer (Create prototypes, write code and test and develop modules and components.)
			또는
		Junior programmer or analyst (Write code based on documented design; modify applications to maintain functionality.)
		에 지원 가능
	그리고
	Unity Certified User 자격증 테스트 가능
		

100. 그 외
카메라 선택 후 CTRL+SHIFT+F => Scene view 에서 보이는 데로 카메라를 맞춤
Speed art: DCC를 이용한 아트 제작 과정을 빠르게 촬영한 것 (유튜브에서 검색가능. ex: speed art maya 등)
Empty game object: 포인트 (위치) 라고 생각하자.
스크립트를 만들때는 GameObject 당 하나라는 생각이 아닌, 기능 스크립트를 만든다고 생각하자.
예) 장애물 이라는 GameObject가 있으면,
	Obstacle.cs 를 만드는게 아닌
	MoveLeft.cs, DestroyOutOfBound.cs 등 각 기능별로 스크립트를 만듬
OntriggerEnter vs OnCollisionEnter
	OntriggerEnter (Object에 물리 개념을 사용하지 않는 경우. IsTrigger를 체크해야함. !!중요!!체크를 하는 순간 Object에서 물리 개념이 사라진다.)
		- 두개가 부딪힌지만 판단할 경우 (ex: 자동차가 골라인을 통과했는지 여부 등)
		- IsTrigger는 둘 다 체크할 필요 없다. 둘 중 하나만 체크해도 둘 다 OnTriggerEnter가 실행됨
	OnCollisionEnter (Object에 물리 개념을 사용할 경우)
		- 두개가 부딪힌 후 물리연산이 필요한 경우

	둘 모두 공통적으로, 누구랑 부딫혔는지 확인하기 위해, 누구의 Tag를 이용한다.
한번에 다 같이 DisActivate 시키거나, InActivate 시켜야 하는 애들은, 빈 Parent Object를 만들어서 넣고, Parent Object만을 껐다가 켰다가 하면 편하다.
Restart 기능을 구현할 때는, SceneManager.Load 로 신을 다시 로드하면 편하다.
타이머를 만들 때, Update 구문내에서 TimeLeft -= Time.deltaTime 을 이용하면, float 단위로 시간이 줄어드는 것을 보여줄 수 있다. Ceil 하면 초단위로.

Mathf.Approximately(float, float): 두 float이 비슷한 값인지 계산한다.
	float a = 1.000000f;
        float b = 1.000001f;	=> true
	float b = 1.000002f;	=> false

101. 내 첫 프로젝트
전체 시간: 24시간 (8시간 X 3일)로 완성되는 걸 만들꺼야.