﻿1. 일을 하면서 아래 항목들을 계속해서 갱신
- 원하는 걸 하기 위해, 자신의 어떤 기술을 발전시켜야 하는지 지표가 됨
- 포트폴리오에서 어떤걸 보여줘야 하는지 알게됨


1) The career(s) you are potentially interested in
	- Software developer (Unity)

2) Specific job descriptions with qualifications within your career(s) area(s)
	- Unity

3) Expected skillsets for each career area, if there is more than one
	- Unity

4) Expected technical knowledge for each career area, if there is more than one
	- Unity

5) Necessary training and/or qualifications
	- Unity

6) Expected salary ranges for specific job
	- 모름