using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    const float zBound = 125f;
    const float force = 1f;

    Rigidbody bulletRb;

    void Awake()
    {
        bulletRb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        bulletRb.AddForce(transform.forward * force, ForceMode.Impulse);

        if (transform.position.z > zBound)
            Destroy(gameObject);
    }

    void OnCollisionEnter(Collision collision)
    {
        if (!collision.gameObject.CompareTag("Gun"))
            Destroy(gameObject);
    }
}
