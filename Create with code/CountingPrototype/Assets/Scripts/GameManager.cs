using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    const int bulletCnt = 10;

    readonly Vector3 directViewPos = new Vector3(0, 0, 0);
    readonly Vector3 fpsViewOffset = new Vector3(-2, 1, -4);

    [SerializeField] GameObject titleScreen;
    [SerializeField] Button directViewBtn;
    [SerializeField] Button startBtn;
    [SerializeField] Button fpsViewBtn;
    [SerializeField] GameObject playScreen;
    [SerializeField] Text bulletCntLeftText;
    [SerializeField] Text timerText;
    [SerializeField] Text hitResultText;
    [SerializeField] GameObject gameOverScreen;
    [SerializeField] Text gameOverResultText;
    [SerializeField] Button restartBtn;
    [SerializeField] GameObject gunSight;
    [SerializeField] Rigidbody targetRb;
    [SerializeField] AudioClip btnClickSound;
    [SerializeField] AudioClip gameOverSound;

    public bool isGamePlaying { get; private set; }

    int timeLeft = 10;
    int bulletCntLeft = bulletCnt;
    int hitCnt = 0;

    void Awake()
    {
        directViewBtn.onClick.AddListener(() =>
        {
            Camera.main.transform.localPosition = Vector3.zero;
            Camera.main.transform.eulerAngles = Vector3.zero;
        });

        startBtn.onClick.AddListener(() =>
        {
            Cursor.lockState = CursorLockMode.Confined;
            Cursor.visible = false;

            titleScreen.SetActive(false);

            UpdateBulletLeftCntText();
            bulletCntLeftText.gameObject.SetActive(true);

            UpdateHitResultText();
            hitResultText.gameObject.SetActive(true);

            playScreen.SetActive(true);

            targetRb.useGravity = true;

            AudioSource.PlayClipAtPoint(btnClickSound, Camera.main.transform.position);

            StartCoroutine(GameOverCountdown());

            isGamePlaying = true;
        });

        fpsViewBtn.onClick.AddListener(() =>
        {
            Camera.main.transform.localPosition = Vector3.zero + fpsViewOffset;
            Camera.main.transform.LookAt(gunSight.transform);
        });

        restartBtn.onClick.AddListener(() =>
        {
            StartCoroutine(RestartGame());
        });
    }

    void GameOver()
    {
        playScreen.SetActive(false);

        gameOverResultText.text = hitResultText.text;

        gameOverScreen.SetActive(true);

        AudioSource.PlayClipAtPoint(gameOverSound, Camera.main.transform.position, 0.5f);

        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    void UpdateBulletLeftCntText()
    {
        bulletCntLeftText.text = string.Format("Bullets left: {0}", bulletCntLeft);
    }

    void UpdateHitResultText()
    {
        hitResultText.text = string.Format("Hit result: {0}/{1} ({2}%)", hitCnt, bulletCnt, Mathf.RoundToInt((float)hitCnt / bulletCnt * 100));
    }

    IEnumerator GameOverCountdown()
    {
        int timeLeft = this.timeLeft;

        while (timeLeft >= 0)
        {
            timerText.text = timeLeft.ToString();
            timeLeft--;

            yield return new WaitForSeconds(1);
        }

        if (isGamePlaying)
            StartCoroutine(FinishGame());
    }

    IEnumerator FinishGame()
    {
        isGamePlaying = false;

        yield return new WaitForSeconds(0.5f);

        GameOver();
    }

    IEnumerator RestartGame()
    {
        AudioSource.PlayClipAtPoint(btnClickSound, Camera.main.transform.position);

        yield return new WaitForSeconds(0.25f);

        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void AddShootCount()
    {
        bulletCntLeft--;

        UpdateBulletLeftCntText();
        UpdateHitResultText();

        if (bulletCntLeft == 0)
            StartCoroutine(FinishGame());
    }

    public void AddHitCount()
    {
        hitCnt++;
        UpdateHitResultText();
    }
}
