using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gun : MonoBehaviour
{
    const float rotationMaxAngle = 20;
    const float rotationSpeed = 1.5f;
    const int fireCntAtOnce = 1;

    [SerializeField] GameObject bulletPrefab;
    [SerializeField] AudioClip shootSound;

    GameManager gameManager;
    Vector3 curRotation;
    bool isFiring;

    void Awake()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (gameManager.isGamePlaying)
        {
            // Set
            curRotation.x += Input.GetAxis("Mouse X") * rotationSpeed;
            curRotation.y += Input.GetAxis("Mouse Y") * rotationSpeed;

            // Adjust
            curRotation.x = Mathf.Clamp(curRotation.x, -rotationMaxAngle, rotationMaxAngle);
            curRotation.y = Mathf.Clamp(curRotation.y, -rotationMaxAngle, rotationMaxAngle);

            // Rotate
            transform.rotation = Quaternion.Euler(-curRotation.y, curRotation.x, 0);

            // Bullet
            if (Input.GetMouseButtonDown(0))
            {
                if (!isFiring)
                    StartCoroutine(Fire());
            }
        }
    }

    IEnumerator Fire()
    {
        isFiring = true;

        for (int cnt = 0; cnt < fireCntAtOnce; cnt++)
        {
            Vector3 pos = transform.position + transform.forward * 1.25f;

            GameObject bullet = Instantiate(bulletPrefab, pos, bulletPrefab.transform.rotation);
            bullet.transform.forward = transform.forward;

            AudioSource.PlayClipAtPoint(shootSound, pos, 0.5f);

            gameManager.AddShootCount();

            yield return new WaitForSeconds(0.1f);
        }

        isFiring = false;
    }
}
