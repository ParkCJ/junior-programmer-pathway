using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PumpBtn : MonoBehaviour
{
    const float pumpInterval = 1f;
    const float pumpScaleMax = 1.2f;

    Coroutine pumpCoroutine;
    bool pump = false;

    // Start is called before the first frame update
    void Start()
    {
        pumpCoroutine = StartCoroutine(PumpStartBtn());
    }

    // Update is called once per frame
    void Update()
    {
        if (pump)
        {
            if (transform.localScale.x < pumpScaleMax)
                transform.localScale += Vector3.one * Time.deltaTime;
            else
                pump = false;
        }
        else
        {
            if (transform.localScale.x > Vector3.one.x)
                transform.localScale -= Vector3.one * Time.deltaTime;
        }
    }

    IEnumerator PumpStartBtn()
    {
        while (true)
        {
            yield return new WaitForSeconds(pumpInterval);

            pump = true;
        }
    }
}
