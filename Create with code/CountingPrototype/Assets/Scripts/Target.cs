using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{
    [SerializeField] ParticleSystem explosionParticle;

    AudioSource targetAudio;
    GameManager gameManager;

    void Awake()
    {
        targetAudio = GetComponent<AudioSource>();
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bullet"))
        {
            explosionParticle.Play();
            targetAudio.Play();

            gameManager.AddHitCount();
        }
    }
}
