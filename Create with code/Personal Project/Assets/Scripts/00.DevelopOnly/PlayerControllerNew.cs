﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerNew : MonoBehaviour
{
    const float speed = 5;
    const float jumpForce = 5;
    const float rotateSpeed = 100;

    Rigidbody objectRb;
    Animator objectAnim;

    bool onGround = true;

    // Start is called before the first frame update
    void Start()
    {
        objectRb = GetComponent<Rigidbody>();
        objectAnim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        DefaultMove();
    }

    // Basic move (Foward, Back like FPS)
    void DefaultMove()
    {
        if (onGround)
        {
            float horInput = Input.GetAxis("Horizontal");
            float vertInput = Input.GetAxis("Vertical");

            objectAnim.SetFloat("Speed_f", vertInput);
            transform.Translate(Vector3.forward * vertInput * Time.deltaTime * speed);

            objectAnim.SetBool("Jump_b", Input.GetKey(KeyCode.Space));
            if (Input.GetKey(KeyCode.Space))
            {
                objectRb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
                objectRb.AddForce(Vector3.forward * vertInput * jumpForce, ForceMode.Impulse);
                onGround = false;
            }
        }
    }

    // Biohazard move
    void Biohazard1Move()
    {
        if (onGround)
        {
            float horInput = Input.GetAxis("Horizontal");
            float vertInput = Input.GetAxis("Vertical");

            objectAnim.SetFloat("Speed_f", vertInput);
            transform.Translate(Vector3.forward * vertInput * Time.deltaTime * speed);
            transform.Rotate(Vector3.up, horInput * Time.deltaTime * rotateSpeed);

            objectAnim.SetBool("Jump_b", Input.GetKey(KeyCode.Space));
            if (Input.GetKey(KeyCode.Space))
            {
                objectRb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
                objectRb.AddRelativeForce(Vector3.forward * vertInput * jumpForce, ForceMode.Impulse);
                onGround = false;
            }
        }
    }

    // All direction move (like Playstation stick)
    void AllMove()
    {
        if (onGround)
        {
            float horInput = Input.GetAxis("Horizontal");
            float vertInput = Input.GetAxis("Vertical");

            Vector3 newFoward = new Vector3(horInput, 0, vertInput).normalized;
            if (newFoward != Vector3.zero)
                transform.forward = newFoward;

            float speedF = 0.0f;
            bool hasHInput = Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow);
            bool hasVInput = Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.DownArrow);

            if (hasHInput && hasVInput)
            {
                speedF = (Mathf.Abs(horInput) + Mathf.Abs(vertInput)) / 2;
            }
            else if (hasHInput)
            {
                speedF = Mathf.Abs(horInput);
            }
            else if (hasVInput)
            {
                speedF = Mathf.Abs(vertInput);
            }
            else
            {
                speedF = 0.0f;
            }

            objectAnim.SetFloat("Speed_f", speedF);
            transform.Translate(Vector3.forward * speedF * Time.deltaTime * speed);

            objectAnim.SetBool("Jump_b", Input.GetKey(KeyCode.Space));
            if (Input.GetKey(KeyCode.Space))
            {
                objectRb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
                objectRb.AddRelativeForce(Vector3.forward * speedF * jumpForce, ForceMode.Impulse);
                onGround = false;
            }
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        onGround = true;
    }
}
