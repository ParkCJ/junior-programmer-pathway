using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimalFlyBlock : NormalBlock
{
    [SerializeField] GameObject animalFlyPrefab;

    protected override void Awake()
    {
        activeDelay = 2f;
        base.Awake();
    }

    protected override IEnumerator StartAction()
    {
        isActivated = true;
        objectRnd.material = activateMaterial;

        yield return new WaitForSeconds(gameCtrl.PlayMode == PlayMode.Intro ? introActiveDelay : activeDelay);

        Instantiate(animalFlyPrefab, transform.position + Vector3.up, animalFlyPrefab.transform.rotation);
        objectRnd.material = grassMaterial;
    }
}