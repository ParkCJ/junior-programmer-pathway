using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimalRunBlock : NormalBlock
{
    [SerializeField] GameObject animalRunPrefab;

    protected override void Awake()
    {
        activeDelay = 2f;
        base.Awake();
    }

    protected override IEnumerator StartAction()
    {
        isActivated = true;
        objectRnd.material = activateMaterial;

        yield return new WaitForSeconds(gameCtrl.PlayMode == PlayMode.Intro ? introActiveDelay : activeDelay);

        Instantiate(animalRunPrefab, transform.position + Vector3.up, animalRunPrefab.transform.rotation);
        objectRnd.material = grassMaterial;
    }
}
