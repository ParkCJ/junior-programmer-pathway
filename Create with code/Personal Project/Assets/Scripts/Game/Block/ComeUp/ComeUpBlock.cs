using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComeUpBlock : NormalBlock
{
    [SerializeField] float upPower;

    [Header("Audio")]
    [SerializeField] AudioClip comeUpSound;
    [SerializeField] AudioClip conflictSound;

    AudioSource objectAudio;
    Rigidbody objectRb;

    bool isMoving = false;

    protected override void Awake()
    {
        activeDelay = 1.5f;
        gameCtrl = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        objectRnd = GetComponent<Renderer>();
        objectRb = GetComponent<Rigidbody>();
        objectAudio = GetComponent<AudioSource>();
    }

    protected override void OnCollisionEnter(Collision collision)
    {
        if (!isActivated && collision.gameObject == gameCtrl.Player)
        {
            StartCoroutine(StartAction());
        }
        else if (isActivated && isMoving && collision.gameObject.CompareTag("Ground"))
        {
            objectAudio.PlayOneShot(conflictSound, 2.0f);
            isMoving = false;
        }
    }

    void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
            isMoving = true;
    }

    protected override IEnumerator StartAction()
    {
        isActivated = true;
        objectRnd.material = activateMaterial;

        yield return new WaitForSeconds(gameCtrl.PlayMode == PlayMode.Intro ? introActiveDelay : activeDelay);

        objectRnd.material = grassMaterial;
        objectRb.AddForce(Vector3.up * upPower, ForceMode.Impulse);

        objectAudio.PlayOneShot(comeUpSound, 2.0f);

        Destroy(plane);
    }
}
