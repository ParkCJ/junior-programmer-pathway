using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallDownBlock : NormalBlock
{
    const float shrinkRate = 0.9f;

    [SerializeField] Material rockMaterial;

    [Header("Audio")]
    [SerializeField] AudioClip fallDownSound;

    protected override void Awake()
    {
        activeDelay = 0.5f;
        base.Awake();
    }

    protected override IEnumerator StartAction()
    {
        isActivated = true;
        objectRnd.material = activateMaterial;

        yield return new WaitForSeconds(gameCtrl.PlayMode == PlayMode.Intro ? introActiveDelay : activeDelay);

        transform.localScale *= shrinkRate;
        objectRnd.material = rockMaterial;
        Destroy(plane);

        AudioSource.PlayClipAtPoint(fallDownSound, transform.position, 6f);
    }
}
