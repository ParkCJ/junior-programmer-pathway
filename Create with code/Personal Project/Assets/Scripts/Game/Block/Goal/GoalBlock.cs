using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalBlock : NormalBlock
{
    protected override void Start()
    {
        // do nothing
    }

    protected override void Update()
    {
        // do nothing
    }

    protected override void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject == gameCtrl.Player && !isActivated)
        {
            isActivated = true;
            gameCtrl.SetGameCleared();
        }
    }
}
