using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalBlock : MonoBehaviour
{
    protected const float introActiveDelay = 3f;

    [Header("Config")]
    [SerializeField] protected Material grassMaterial;
    [SerializeField] protected Material activateMaterial;
    [SerializeField] protected GameObject plane;

    protected GameController gameCtrl;
    protected Renderer objectRnd;

    protected float activeDelay = 1f;
    protected float introStartDelay = 90f;
    protected bool isActivated = false;

    protected virtual void Awake()
    {
        gameCtrl = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        objectRnd = GetComponent<Renderer>();
    }

    protected virtual void Start()
    {
        if (gameCtrl.PlayMode == PlayMode.Intro)
        {
            StartCoroutine(StartIntroModeAction());
        }
    }

    protected virtual void Update()
    {
        if (gameCtrl.GameCleared)
            StopAllCoroutines();
    }

    protected virtual void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject == gameCtrl.Player && !isActivated)
        {
            StartCoroutine(StartAction());
        }
    }

    protected virtual IEnumerator StartAction()
    {
        isActivated = true;
        objectRnd.material = grassMaterial;

        yield return new WaitForSeconds(gameCtrl.PlayMode == PlayMode.Intro ? introActiveDelay : activeDelay);

        // do nothing
    }

    protected virtual IEnumerator StartIntroModeAction()
    {
        yield return new WaitForSeconds(Random.Range(0, introStartDelay));

        StartCoroutine(StartAction());
    }
}
