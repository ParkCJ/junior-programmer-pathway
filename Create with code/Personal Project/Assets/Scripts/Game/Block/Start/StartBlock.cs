using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartBlock : NormalBlock
{
    protected override void Awake()
    {
    }

    protected override void Start()
    {
    }

    protected override void Update()
    {
    }

    protected override void OnCollisionEnter(Collision collision)
    {
    }
}
