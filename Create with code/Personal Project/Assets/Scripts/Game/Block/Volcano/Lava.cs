using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lava : MonoBehaviour
{
    [Header("Config")]
    [SerializeField] float pushForce;

    [Header("Particle")]
    [SerializeField] ParticleSystem lavaExplosion;

    [Header("Audio")]
    [SerializeField] AudioClip explosionSound;

    GameController gameCtrl;

    void Awake()
    {
        gameCtrl = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
    }

    void OnCollisionEnter(Collision collision)
    {
        Rigidbody collisionRb = collision.gameObject.GetComponent<Rigidbody>();
        if (collisionRb != null && !collision.gameObject.CompareTag("Ground") && !collision.gameObject.CompareTag("Lava"))
        {
            Vector3 direction = (collision.gameObject.transform.position - transform.position).normalized;
            direction = new Vector3(direction.x, 0, direction.z);

            Instantiate(lavaExplosion, transform.position, lavaExplosion.transform.rotation);
            collisionRb.AddForce(direction * pushForce, ForceMode.Impulse);

            AudioSource.PlayClipAtPoint(explosionSound, transform.position, 2.0f);

            Destroy(gameObject, explosionSound.length);
        }
    }
}
