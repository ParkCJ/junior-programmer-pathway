using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpoutOut : MonoBehaviour
{
    [Header("Config")]
    [SerializeField] float spoutForce;

    const float maxDirection = 0.3f;
    const float maxTorque = 100f;

    Rigidbody objectRb;

    void Awake()
    {
        objectRb = GetComponent<Rigidbody>();

        objectRb.AddForce(RandomDirection() * spoutForce, ForceMode.Impulse);
        objectRb.AddTorque(RandomTorque(), RandomTorque(), RandomTorque(), ForceMode.Impulse);
    }

    Vector3 RandomDirection()
    {
        float randomX = Random.Range(-maxDirection, maxDirection);
        float randomZ = Random.Range(-maxDirection, maxDirection);

        return new Vector3(randomX, 1, randomZ);
    }

    float RandomTorque()
    {
        return Random.Range(-maxTorque, maxTorque);
    }
}
