using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VolcanoBlock : NormalBlock
{
    const float groundLaunchInterval = 0.25f;
    const float firstExplosionInterval = 2f;
    const int firstExplosionLavaCnt = 3;
    const int spoutOutLavaCntMin = 1;
    const int spoutOutLavaCntMax = 2;
    const float earthquakeRate = 1.075f;
    const float spoutOutIntervalMin = 0.5f;
    const float spoutOutIntervalMax = 1.0f;
    const float introModeSpoutOutIntervalMin = 10f;
    const float introModeSpoutOutIntervalMax = 30f;

    [Header("Volcano")]
    [SerializeField] GameObject groundLev2;
    [SerializeField] GameObject groundLev3;
    [SerializeField] GameObject groundTop;
    [SerializeField] GameObject lavaPrefab;
    [SerializeField] Material volcanoMaterial;

    [Header("Particle")]
    [SerializeField] ParticleSystem topFlame;

    [Header("Audio")]
    [SerializeField] AudioClip firstExplosionSound;
    [SerializeField] AudioClip spoutOutSound;

    AudioSource volcanoAudio;
    Vector3 defaultScale;

    protected override void Awake()
    {
        activeDelay = 1f;
        introStartDelay = 30f;
        gameCtrl = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        objectRnd = GetComponent<Renderer>();
        volcanoAudio = GetComponent<AudioSource>();

        defaultScale = transform.localScale;
    }

    void LaunchVolcano()
    {
        objectRnd.material = volcanoMaterial;
        StartCoroutine(LaunchGroundLev2());
    }

    IEnumerator LaunchGroundLev2()
    {
        yield return new WaitForSeconds(groundLaunchInterval);

        groundLev2.SetActive(true);
        StartCoroutine(LaunchGroundLev3());
    }

    IEnumerator LaunchGroundLev3()
    {
        yield return new WaitForSeconds(groundLaunchInterval);

        groundLev3.SetActive(true);
        StartCoroutine(LaunchGroundTop());
    }

    IEnumerator LaunchGroundTop()
    {
        yield return new WaitForSeconds(groundLaunchInterval);

        groundTop.SetActive(true);
        StartCoroutine(FirstExplosion());
    }

    IEnumerator FirstExplosion()
    {
        yield return new WaitForSeconds(firstExplosionInterval);

        if (!gameCtrl.GameCleared)
        {
            if (gameCtrl.PlayMode == PlayMode.Play)
                transform.localScale = transform.localScale * earthquakeRate;

            int lavaCount = firstExplosionLavaCnt;
            while (lavaCount > 0)
            {
                CreateLava();
                lavaCount--;
            }

            topFlame.gameObject.SetActive(true);
            volcanoAudio.PlayOneShot(firstExplosionSound, 2.0f);

            StartCoroutine(AfterFistExplosion());
        }
    }

    IEnumerator AfterFistExplosion()
    {
        yield return new WaitForFixedUpdate();

        transform.localScale = defaultScale;

        StartCoroutine(SpoutOutLava());
    }

    IEnumerator SpoutOutLava()
    {
        while (!gameCtrl.GameCleared)
        {
            float spoutDuration;
            if (gameCtrl.PlayMode == PlayMode.Intro)
                spoutDuration = Random.Range(introModeSpoutOutIntervalMin, introModeSpoutOutIntervalMax);
            else
                spoutDuration = Random.Range(spoutOutIntervalMin, spoutOutIntervalMax);

            yield return new WaitForSeconds(spoutDuration);

            int lavaCount = Random.Range(spoutOutLavaCntMin, spoutOutLavaCntMax + 1);
            while (lavaCount > 0)
            {
                CreateLava();
                lavaCount--;
            }
        }
    }

    void CreateLava()
    {
        Instantiate(lavaPrefab, groundTop.transform.position + Vector3.up, lavaPrefab.transform.rotation);
        volcanoAudio.PlayOneShot(spoutOutSound, 2.0f);
    }

    protected override IEnumerator StartAction()
    {
        isActivated = true;
        objectRnd.material = activateMaterial;

        yield return new WaitForSeconds(gameCtrl.PlayMode == PlayMode.Intro ? introActiveDelay : activeDelay);

        LaunchVolcano();
    }
}