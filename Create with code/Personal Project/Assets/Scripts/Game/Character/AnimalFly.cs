using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimalFly : MonoBehaviour
{
    public enum FlyType
    {
        WakeUp,
        GoUp,
        AroundThePlayer,
        FallToPlayer
    }

    const float yMinRange = 1.5f;
    const float yMaxRange = 5.0f;

    [Header("Config")]
    [SerializeField] float wakeupTime;
    [SerializeField] float goUpSpeed;
    [SerializeField] float goUpY;
    [SerializeField] float flyAroundSpeed;
    [SerializeField] float flyAroundTime;
    [SerializeField] float fallSpeed;
    [SerializeField] float pushForce;
    [SerializeField] float normalForce;

    [Header("Audio")]
    [SerializeField] AudioClip wakeUpSound;
    [SerializeField] AudioClip attackSound;
    [SerializeField] AudioClip flyingUpSound;
    [SerializeField] AudioClip fallDownSound;

    [Header("Particle")]
    [SerializeField] ParticleSystem featherEffect;

    GameController gameCtrl;
    Rigidbody objectRb;
    AudioSource objectAudio;

    FlyType flyType = FlyType.WakeUp;
    Vector3 newFoward;

    void Awake()
    {
        gameCtrl = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        objectRb = GetComponent<Rigidbody>();
        objectAudio = GetComponent<AudioSource>();
    }

    void Start()
    {
        newFoward = (gameCtrl.Player.transform.position - transform.position).normalized;
        if (newFoward != Vector3.zero)
            transform.forward = newFoward;

        StartCoroutine(WakeUp());
    }

    IEnumerator WakeUp()
    {
        objectAudio.PlayOneShot(wakeUpSound, 2.0f);

        yield return new WaitForSeconds(wakeupTime);

        flyType = FlyType.GoUp;
    }

    void FixedUpdate()
    {
        if (flyType == FlyType.GoUp)
        {
            if (transform.position.y > yMaxRange)
            {
                objectRb.velocity = Vector3.zero;
                objectRb.angularVelocity = Vector3.zero;

                flyType = FlyType.AroundThePlayer;

                if (gameCtrl.PlayMode == PlayMode.Play)
                    StartCoroutine(StartFallToPlayer());
            }
            else
            {
                Vector3 direction = new Vector3(newFoward.x, goUpY, newFoward.z);
                if (direction != Vector3.zero)
                    transform.forward = direction;
                objectRb.AddRelativeForce(Vector3.forward * goUpSpeed);

                if (!objectAudio.isPlaying)
                    objectAudio.PlayOneShot(flyingUpSound, 0.6f);
            }
        }
        else if (flyType == FlyType.AroundThePlayer)
        {
            newFoward = (gameCtrl.Player.transform.position - transform.position).normalized;
            newFoward = new Vector3(newFoward.x, 0, newFoward.z);
            if (newFoward != Vector3.zero)
                transform.forward = newFoward;

            Vector3 dirction = new Vector3(Vector3.forward.x, 0, Vector3.forward.z);
            objectRb.AddRelativeForce(dirction * flyAroundSpeed);
        }
        else if (flyType == FlyType.FallToPlayer)
        {
            if (transform.position.y < yMinRange)
            {
                flyType = FlyType.GoUp;
            }
            else
            {
                newFoward = (gameCtrl.Player.transform.position - transform.position).normalized;
                if (newFoward != Vector3.zero)
                    transform.forward = newFoward;

                objectRb.AddRelativeForce(Vector3.forward * fallSpeed);

                if (featherEffect != null)
                    Instantiate(featherEffect, transform.position, featherEffect.transform.rotation);
            }
        }
    }

    IEnumerator StartFallToPlayer()
    {
        yield return new WaitForSeconds(flyAroundTime);

        objectRb.velocity = Vector3.zero;
        objectRb.angularVelocity = Vector3.zero;

        flyType = FlyType.FallToPlayer;

        if (!objectAudio.isPlaying)
            objectAudio.PlayOneShot(fallDownSound, 0.6f);
    }

    void OnCollisionEnter(Collision collision)
    {
        Rigidbody collisionRb = collision.gameObject.GetComponent<Rigidbody>();
        if (collisionRb != null)
        {
            Vector3 direction = (collision.gameObject.transform.position - transform.position).normalized;
            direction = new Vector3(direction.x, 0, direction.z);

            if (flyType == FlyType.FallToPlayer)
            {
                collisionRb.AddForce(direction * pushForce, ForceMode.Impulse);
                objectAudio.PlayOneShot(attackSound, 2.0f);
            }
            else
                collisionRb.AddForce(direction * normalForce, ForceMode.Impulse);

            flyType = FlyType.GoUp;
        }
    }
}
