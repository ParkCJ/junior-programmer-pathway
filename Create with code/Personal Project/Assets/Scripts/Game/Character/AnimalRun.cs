using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimalRun : MonoBehaviour
{
    public enum RunType
    {
        WakeUp,
        Run
    }

    const float minDistance = 0.4f;

    [Header("Config")]
    [SerializeField] float speed;
    [SerializeField] float wakeupTime;
    [SerializeField] float pushForce;
    [SerializeField] float normalForce;

    [Header("Audio")]
    [SerializeField] AudioClip wakeUpSound;
    [SerializeField] AudioClip attackSound;
    [SerializeField] AudioClip runningSound;

    GameController gameCtrl;
    DestroyMe destroyMe;
    Rigidbody objectRb;
    Animator objectAnim;
    AudioSource objectAudio;

    Vector3 newFoward;
    RunType runType = RunType.WakeUp;

    void Awake()
    {
        gameCtrl = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        destroyMe = GetComponent<DestroyMe>();
        objectRb = GetComponent<Rigidbody>();
        objectAnim = transform.Find("Animal").GetComponent<Animator>();
        objectAudio = GetComponent<AudioSource>();
    }

    void Start()
    {
        newFoward = (gameCtrl.Player.transform.position - transform.position).normalized;
        newFoward = new Vector3(newFoward.x, 0, newFoward.z);
        if (newFoward != Vector3.zero)
            transform.forward = newFoward;

        StartCoroutine(WakeUp());
    }

    IEnumerator WakeUp()
    {
        objectAudio.PlayOneShot(wakeUpSound, 2.0f);

        objectAnim.SetFloat("Speed_f", 0);
        objectAnim.SetBool("Eat_b", true);

        yield return new WaitForSeconds(wakeupTime);

        switch(gameCtrl.PlayMode)
        {
            case PlayMode.Intro:
                // do nothing
                break;
            case PlayMode.Play:
            default:
                objectAnim.SetFloat("Speed_f", 1f);
                objectAnim.SetBool("Eat_b", false);
                runType = RunType.Run;
                break;
        }
    }

    void FixedUpdate()
    {
        if (runType == RunType.Run)
        {
            newFoward = (gameCtrl.Player.transform.position - transform.position).normalized;
            newFoward = new Vector3(newFoward.x, 0, newFoward.z);
            if (newFoward != Vector3.zero)
                transform.forward = newFoward;

            transform.Translate(Vector3.forward * Time.deltaTime * speed, Space.Self);

            if (!objectAudio.isPlaying)
                objectAudio.PlayOneShot(runningSound, 0.3f);
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        Rigidbody collisionRb = collision.gameObject.GetComponent<Rigidbody>();
        if (collisionRb != null)
        {
            Vector3 direction = (collision.gameObject.transform.position - transform.position).normalized;
            direction = new Vector3(direction.x, 0, direction.z);

            if (collision.gameObject.CompareTag("Player"))
            {
                if (IsOnSamePos(collision.gameObject))
                {
                    destroyMe.DestroySelf();
                }
                else
                {
                    if (runType == RunType.Run)
                    {
                        collisionRb.AddForce(direction * pushForce, ForceMode.Impulse);
                        objectAudio.PlayOneShot(attackSound, 3f);
                    }
                    else
                        collisionRb.AddForce(direction * normalForce, ForceMode.Impulse);
                }
            }
            else if (collision.gameObject.CompareTag("AnimalRun"))
            {
                if (IsOnSamePos(collision.gameObject))
                {
                    destroyMe.DestroySelf();
                }
                else
                    collisionRb.AddForce(direction * normalForce, ForceMode.Impulse);
            }
            else
            {
                collisionRb.AddForce(direction * normalForce, ForceMode.Impulse);
            }
        }
    }

    bool IsOnSamePos(GameObject target)
    {
        bool isOnTarget = transform.position.y > target.transform.position.y;
        bool isOnSamePos;

        Vector3 pos = new Vector3(transform.position.x, 0, transform.position.z);
        Vector3 targetPos = new Vector3(target.transform.position.x, 0, target.transform.position.z);

        isOnSamePos = (pos - targetPos).sqrMagnitude < minDistance * minDistance;

        if (isOnTarget && isOnSamePos)
            return true;
        else
            return false;
    }
}
