using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    const float groundDistanceMax = 0.5f;
    const float fallResetTime = 1.0f;
    const float burnResetTime = 0.25f;

    [Header("Config")]
    [SerializeField] float speed;
    [SerializeField] float jumpUpForce;
    [SerializeField] float jumpFowardForce;

    [Header("Audio")]
    [SerializeField] AudioClip jumpSound;
    [SerializeField] AudioClip runningSound;

    GameController gameCtrl;
    Rigidbody playerRb;
    Animator playerAnim;
    AudioSource playerAudio;
    Vector3 startPos;

    bool isOnGround = false;
    bool isReset = false;

    void Awake()
    {
        gameCtrl = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        playerRb = GetComponent<Rigidbody>();
        playerAnim = transform.Find("Girl").GetComponent<Animator>();
        playerAudio = GetComponent<AudioSource>();
        startPos = transform.position;
    }

    void Start()
    {
        if (gameCtrl.PlayMode == PlayMode.Intro)
        {
            playerAnim.SetBool("Dance", true);
        }
    }

    void FixedUpdate()
    {
        if (gameCtrl.GameLoaded)
        {
            ConstraintPlayerPosition();

            if (gameCtrl.PlayMode == PlayMode.Play)
                MovePlayer();
        }
    }

    void ConstraintPlayerPosition()
    {
        if (!gameCtrl.GamePlaying)
        {
            playerAnim.SetFloat("Speed_f", 0);
            playerAnim.SetBool("Jump_b", false);

            if (gameCtrl.GameCleared)
            {
                transform.forward = Vector3.back;
                playerAnim.SetBool("Dance", true);
            }
            else
            {
                playerAnim.SetBool("Death_b", true);
                playerRb.constraints = RigidbodyConstraints.FreezeAll;
            }
        }

        if (gameCtrl.IsUnderStage(transform.position.y) && !isReset)
            StartCoroutine(Fall());
    }

    void MovePlayer()
    {
        if (gameCtrl.GamePlaying && !isReset && isOnGround)
        {
            float horInput = Input.GetAxis("Horizontal");
            float vertInput = Input.GetAxis("Vertical");

            Vector3 newFoward = new Vector3(horInput, 0, vertInput).normalized;
            if (newFoward != Vector3.zero)
                transform.forward = newFoward;

            float speedF = 0.0f;
            bool hasHInput = Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow);
            bool hasVInput = Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.DownArrow);

            if (hasHInput && hasVInput)
                speedF = (Mathf.Abs(horInput) + Mathf.Abs(vertInput)) / 2;
            else if (hasHInput)
                speedF = Mathf.Abs(horInput);
            else if (hasVInput)
                speedF = Mathf.Abs(vertInput);
            else
                speedF = 0.0f;

            playerAnim.SetFloat("Speed_f", speedF);
            transform.Translate(Vector3.forward * speedF * Time.deltaTime * speed);
            if (speedF > 0.5f && !playerAudio.isPlaying)
                playerAudio.PlayOneShot(runningSound, 2.0f);

            if (Input.GetKey(KeyCode.Space) && CanJump())
            {
                playerAnim.SetBool("Jump_b", true);
                playerRb.AddForce(Vector3.up * jumpUpForce, ForceMode.Impulse);
                playerRb.AddRelativeForce(Vector3.forward * speedF * jumpFowardForce, ForceMode.Impulse);

                playerAudio.PlayOneShot(jumpSound, 2.0f);
                isOnGround = false;
            }
            else
                playerAnim.SetBool("Jump_b", false);
        }
    }

    bool CanJump()
    {
        if (Physics.Raycast(transform.position, Vector3.down, groundDistanceMax))
            return true;
        else
            return false;
    }

    IEnumerator Fall()
    {
        isReset = true;

        yield return new WaitForSeconds(fallResetTime);

        Reset();
    }

    IEnumerator Burn()
    {
        isReset = true;

        yield return new WaitForSeconds(burnResetTime);

        Reset();
    }

    void Reset()
    {
        isOnGround = false;

        playerRb.velocity = Vector3.zero;
        playerRb.angularVelocity = Vector3.zero;

        gameCtrl.DecreaseLife();

        transform.position = startPos;
        transform.forward = Vector3.forward;

        isReset = false;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ground") || collision.gameObject.CompareTag("AnimalRun") || collision.gameObject.CompareTag("AnimalFly"))
        {
            isOnGround = true;
        }
        else if (collision.gameObject.CompareTag("Lava"))
        {
            if (!isReset)
                StartCoroutine(Burn());
        }
    }
}