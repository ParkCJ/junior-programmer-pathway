using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyMe : MonoBehaviour
{
    const float destroyRotationSpeed = 100.0f;
    const float destroyShrinkSpeed = 0.99f;
    const float destroyScale = 0.3f;

    [Header("Config")]
    [SerializeField] bool useLifeTime;
    [SerializeField] float lifeTime;
    [SerializeField] float yDestroyRange;
    [SerializeField] bool destroyByLava;

    [Header("Effect")]
    [SerializeField] ParticleSystem destroyParticle;
    [SerializeField] AudioClip destroySound;

    GameController gameCtrl;
    Rigidbody objectRb;

    bool startDestroy = false;
    bool isDestroying = false;

    void Awake()
    {
        gameCtrl = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        objectRb = GetComponent<Rigidbody>();
    }

    // Start is called before the first frame update
    void Start()
    {
        if (useLifeTime)
            StartCoroutine(StartDestroy());
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (gameCtrl.GameCleared && useLifeTime)
            startDestroy = true;

        if (startDestroy)
        {
            if (!isDestroying)
            {
                Instantiate(destroyParticle, transform.position, destroyParticle.transform.rotation);

                if (objectRb != null)
                {
                    objectRb.velocity = Vector3.zero;
                    objectRb.angularVelocity = Vector3.zero;
                }

                // destroy all scripts
                foreach (MonoBehaviour script in GetComponents<MonoBehaviour>())
                {
                    if (script != this)
                        Destroy(script);
                }

                isDestroying = true;
            }

            transform.Rotate(Vector3.up, destroyRotationSpeed * Time.deltaTime);
            transform.localScale *= destroyShrinkSpeed;

            if (transform.localScale.x < destroyScale)
                DestroyNow();
        }
        else if (transform.position.y < yDestroyRange)
        {
            Destroy(gameObject);
        }
    }

    IEnumerator StartDestroy()
    {
        yield return new WaitForSeconds(lifeTime);

        Instantiate(destroyParticle, transform.position, destroyParticle.transform.rotation);
        startDestroy = true;
    }

    void DestroyNow()
    {
        AudioSource.PlayClipAtPoint(destroySound, transform.position, 2.0f);
        Destroy(gameObject);
    }

    public void DestroySelf()
    {
        startDestroy = true;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (destroyByLava && collision.gameObject.CompareTag("Lava"))
            DestroyNow();
    }
}
