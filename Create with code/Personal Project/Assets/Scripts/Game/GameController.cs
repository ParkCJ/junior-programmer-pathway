using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [Header("Audio")]
    [SerializeField] AudioClip gameClearSound;
    [SerializeField] AudioClip gameOverSound;
    [SerializeField] AudioClip lifeDownSound;

    [Header("Player")]
    [SerializeField] GameObject playerPrefab;

    [Header("Stage")]
    [SerializeField] GameObject stage;

    [Header("Block")]
    [SerializeField] GameObject startBlockPrefab;
    [SerializeField] GameObject goalBlockPrefab;
    [SerializeField] GameObject normalBlockPrefab;
    [SerializeField] GameObject[] obstacle1x1s; // 1x1 size obstacles
    [SerializeField] GameObject[] obstacle2x2s; // 2x2 size obstacles
    [SerializeField] GameObject[] obstacle3x3s; // 3x3 size obstacles
    [SerializeField] GameObject[] obstacle4x4s; // 4x4 size obstacles
    [SerializeField] GameObject[] obstacle5x5s; // 5x5 size obstacles

    [Header("Camera")]
    [SerializeField] Camera actionCam;

    public ActionCamType ActionCamType { get; private set; }
    public RenderTexture ActionCamRndTexture { get; private set; }
    public GameObject Player { get; private set; }
    public bool GameLoaded { get; private set; }
    public bool GamePlaying { get; private set; }
    public bool GameCleared { get; private set; }
    public PlayMode PlayMode { get; private set; }
    public Difficulty Difficulty { get; private set; }
    public int LifeCnt { get; private set; }

    GameManager gameManager;
    GameObject camRotationAnchor;
    Dictionary<int, Dictionary<int, bool>> haveBlockBook;
    Vector3 stageCenterPos;
    int stageSizeX;
    int stageSizeZ;
    int defaultLifeCnt;
    float obstacleRate;
    int volcanoCnt;

    private void Awake()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
    }

    public void CreateGame(PlayMode playMode, Difficulty difficulty, int lifeCnt, int stageSizeX, int stageSizeZ, float obstacleRate, int volcanoCnt)
    {
        ConfigGame(playMode, difficulty, lifeCnt, stageSizeX, stageSizeZ, obstacleRate, volcanoCnt);
        CreatePlayer();
        CreateStage();
        CreateActionCam();

        GameLoaded = true;
        GamePlaying = true;
    }

    void ConfigGame(PlayMode playMode, Difficulty difficulty, int lifeCnt, int stageSizeX, int stageSizeZ, float obstacleRate, int volcanoCnt)
    {
        this.PlayMode = playMode;
        this.Difficulty = difficulty;
        this.defaultLifeCnt = lifeCnt;
        this.LifeCnt = lifeCnt;
        this.stageSizeX = stageSizeX;
        this.stageSizeZ = stageSizeZ;
        this.obstacleRate = obstacleRate;
        this.volcanoCnt = volcanoCnt;

        // Center position
        stageCenterPos = new Vector3((stageSizeX - GameConfig.Block1x1Height) / 2, GameConfig.Block1x1Height / 2, (stageSizeZ - GameConfig.Block1x1Height) / 2);
        
        // HaveBlockBook
        haveBlockBook = new Dictionary<int, Dictionary<int, bool>>();
        for (int xPos = 0; xPos < stageSizeX; xPos++)
        {
            haveBlockBook[xPos] = new Dictionary<int, bool>();
            for (int zPos = 0; zPos < stageSizeZ; zPos++)
                haveBlockBook[xPos][zPos] = false;
        }

        // Camera
        switch (playMode)
        {
            case PlayMode.Intro:
                camRotationAnchor = new GameObject("CamRotationAnchor");
                camRotationAnchor.transform.position = stageCenterPos;
                Camera.main.transform.position = GameConfig.IntroCamPosition;
                Camera.main.transform.rotation = Quaternion.Euler(GameConfig.IntroCamRotation);
                Camera.main.transform.parent = camRotationAnchor.transform;
                break;
            case PlayMode.Play:
                switch (difficulty)
                {
                    case Difficulty.Beginner:
                        Camera.main.transform.position = GameConfig.BeginnerCamPosition;
                        Camera.main.transform.rotation = Quaternion.Euler(GameConfig.BeginnerCamRotation);
                        break;
                    case Difficulty.Easy:
                        Camera.main.transform.position = GameConfig.EasyCamPosition;
                        Camera.main.transform.rotation = Quaternion.Euler(GameConfig.EasyCamRotation);
                        break;
                    case Difficulty.Normal:
                        Camera.main.transform.position = GameConfig.NormalCamPosition;
                        Camera.main.transform.rotation = Quaternion.Euler(GameConfig.NormalCamRotation);
                        break;
                    case Difficulty.Hard:
                        Camera.main.transform.position = GameConfig.HardCamPosition;
                        Camera.main.transform.rotation = Quaternion.Euler(GameConfig.HardCamRotation);
                        break;
                    case Difficulty.Custom:
                        // Will be created later
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
    }

    void CreatePlayer()
    {
        Player = Instantiate(playerPrefab, GameConfig.PlayerDefPosition, playerPrefab.transform.rotation, gameObject.transform);
    }

    void CreateStage()
    {
        CreateStartBlock();
        CreateGoalBlock();

        // Create game blocks
        for (int xPos = 0;xPos < stageSizeX; xPos++)
        {
            for (int zPos = 0; zPos < stageSizeZ; zPos++)
            {
                if (haveBlockBook[xPos][zPos])
                    continue;
                else
                {
                    int xSpaceLeft = stageSizeX - xPos;
                    int zSpaceLeft = stageSizeZ - zPos;
                    int minSpaceLeft = Mathf.Min(xSpaceLeft, zSpaceLeft);

                    CreateBlock(xPos, zPos, minSpaceLeft);
                }
            }
        }
    }

    void CreateStartBlock()
    {
        Instantiate(startBlockPrefab, GameConfig.StartBlockDefPosition, startBlockPrefab.transform.rotation, stage.transform);
        haveBlockBook[(int)GameConfig.StartBlockDefPosition.x][(int)GameConfig.StartBlockDefPosition.z] = true;
    }

    void CreateGoalBlock()
    {
        Instantiate(goalBlockPrefab, new Vector3(stageSizeX - 1, 0, stageSizeZ - 1), goalBlockPrefab.transform.rotation, stage.transform);
        haveBlockBook[stageSizeX - 1][stageSizeZ - 1] = true;
    }

    void CreateBlock(int xPos, int zPos, int maxSpace)
    {
        int rndRangeMax = stageSizeX * stageSizeZ;
        float obstacleRangeMax = stageSizeX * stageSizeZ * obstacleRate;

        if (Random.Range(1, rndRangeMax + 1) <= obstacleRangeMax)
        {
            CreateObstacle(xPos, zPos, maxSpace);
        }
        else
        {
            CreateNormal(xPos, zPos);
        }
    }

    void CreateNormal (int xPos, int zPos)
    {
        Instantiate(normalBlockPrefab, new Vector3(xPos, 0, zPos), normalBlockPrefab.transform.rotation, stage.transform);
        haveBlockBook[xPos][zPos] = true;
    }

    void CreateObstacle(int xPos, int zPos, int maxSpace)
    {
        if (maxSpace > GameConfig.MaxBlockLength)
            maxSpace = GameConfig.MaxBlockLength;

        int obstacleSize = Random.Range(1, maxSpace + 1);

        switch (obstacleSize)
        {
            case 1:
                InstantiateObstacle(xPos, zPos, obstacleSize, obstacle1x1s);
                break;
            case 2:
                InstantiateObstacle(xPos, zPos, obstacleSize, obstacle2x2s);
                break;
            case 3:
                InstantiateObstacle(xPos, zPos, obstacleSize, obstacle3x3s);
                break;
            case 4:
                InstantiateObstacle(xPos, zPos, obstacleSize, obstacle4x4s);
                break;
            case 5:
                InstantiateObstacle(xPos, zPos, obstacleSize, obstacle5x5s);
                break;
        }
    }

    void InstantiateObstacle(int xPos, int zPos, int obstacleSize, GameObject[] obstacles)
    {
        if (obstacles.Length == 0 || haveBlockBook[xPos][zPos + obstacleSize - 1])
        {   
            CreateObstacle(xPos, zPos, obstacleSize - 1);
        }
        else
        {
            int idx = Random.Range(0, obstacles.Length);

            string obstacleName = obstacles[idx].name;
            if (obstacleName == "VolcanoBlock" && volcanoCnt == 0)
            {
                CreateObstacle(xPos, zPos, obstacleSize);
            }
            else
            {
                Instantiate(obstacles[idx], new Vector3(xPos, 0, zPos), obstacles[idx].transform.rotation, stage.transform);

                for (int x = xPos; x < xPos + obstacleSize; x++)
                    for (int z = zPos; z < zPos + obstacleSize; z++)
                        haveBlockBook[x][z] = true;

                if (obstacleName == "VolcanoBlock")
                    volcanoCnt--;
            }
        }
    }

    void CreateActionCam()
    {
        ActionCamRndTexture = new RenderTexture(256, 256, 24);
        ActionCamRndTexture.name = System.Guid.NewGuid().ToString();
        ActionCamRndTexture.Create();
        actionCam.targetTexture = ActionCamRndTexture;

        SetSecCamAngle(ActionCamType.ACTION);
    }

    public void SetSecCamAngle(ActionCamType type)
    {
        switch (type)
        {
            case ActionCamType.ACTION:
                ActionCamType = type;
                actionCam.transform.parent = Player.transform;
                actionCam.transform.forward = Player.transform.forward;
                actionCam.transform.position = Player.transform.position;
                actionCam.transform.localPosition += new Vector3(0, 0.3f, -1);
                actionCam.transform.Rotate(transform.right * 5);
                break;
            case ActionCamType.TOP:
                ActionCamType = type;
                actionCam.transform.parent = gameObject.transform;
                actionCam.transform.position = stageCenterPos;
                actionCam.transform.position += Vector3.up * Mathf.Max(stageSizeX, stageSizeZ) * 1.5f;
                actionCam.transform.rotation = Quaternion.Euler(Vector3.zero);
                actionCam.transform.Rotate(transform.right * 90);
                break;
        }
    }

    public void SetGameCleared()
    {
        GamePlaying = false;
        GameCleared = true;

        AudioSource.PlayClipAtPoint(gameClearSound, Player.transform.position, 0.8f);

        gameManager.UpdateProgress(Difficulty);
    }

    public void DecreaseLife()
    {
        if (PlayMode == PlayMode.Play && GamePlaying)
        {
            if (LifeCnt > 0)
            {
                LifeCnt--;
                AudioSource.PlayClipAtPoint(lifeDownSound, Player.transform.position, 2.0f);

                if (LifeCnt == 0)
                {
                    GamePlaying = false;
                    AudioSource.PlayClipAtPoint(gameOverSound, Player.transform.position, 1f);
                }
            }
        }
    }

    public void ContinueGame()
    {
        GamePlaying = true;
        LifeCnt = defaultLifeCnt;

        RevivePlayer();
    }

    void RevivePlayer()
    {
        if (ActionCamType == ActionCamType.ACTION)
            actionCam.transform.parent = gameObject.transform;
        Destroy(Player);

        CreatePlayer();
        if (ActionCamType == ActionCamType.ACTION)
            actionCam.transform.parent = Player.transform;
    }

    public bool IsUnderStage(float targetYPos)
    {
        if (targetYPos < GameConfig.Block1x1Height)
        {
            return true;
        }   
        else
        {
            return false;
        }
    }

    void LateUpdate()
    {
        if (GameLoaded)
        {
            if (GamePlaying && PlayMode == PlayMode.Intro)
                camRotationAnchor.transform.Rotate(Vector3.up * GameConfig.StageRotationSpeed * Time.deltaTime);
        }
    }

    void OnDestroy()
    {
        if (ActionCamRndTexture != null)
            ActionCamRndTexture.Release();
    }
}
