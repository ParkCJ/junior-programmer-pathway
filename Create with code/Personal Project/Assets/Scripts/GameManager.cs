﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [Header("UI Fonts")]
    [SerializeField] Font jaDefaultFont;
    [SerializeField] Font enDefaultFont;
    [SerializeField] Font koDefaultFont;
    [SerializeField] Font jaFont;
    [SerializeField] Font enFont;
    [SerializeField] Font koFont;

    [Header("Game")]
    [SerializeField] GameController gameCtrlPrefab;

    public Dictionary<Difficulty, bool> GameProgress { get; private set; }
    GameConfig gameConf;
    Dictionary<string, Dictionary<string, Dictionary<Language, UITextSet>>> uiTextDic;

    void Awake()
    {
        // DontDestroyObject
        GameObject[] objs = GameObject.FindGameObjectsWithTag("GameManager");

        if (objs.Length > 1)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);

        // Game config
        gameConf = new GameConfig();

        // UI text
        uiTextDic = new Dictionary<string, Dictionary<string, Dictionary<Language, UITextSet>>>();
        GetUITextDic();

        // Game progress
        GameProgress = new Dictionary<Difficulty, bool>();
        GameProgress[Difficulty.Beginner] = false;
        GameProgress[Difficulty.Easy] = false;
        GameProgress[Difficulty.Normal] = false;
        GameProgress[Difficulty.Hard] = false;
    }

    public void ChangeLanguage(Language lang)
    {
        gameConf.SetConfig(lang);
        SetUIText();
    }

    public void ChangeConfig(Difficulty difficulty, PlayMode playMode = PlayMode.Play)
    {
        gameConf.SetConfig(difficulty, playMode);
    }

    public GameController CreateGame()
    {
        GameController gameCtrl = Instantiate(gameCtrlPrefab);
        gameCtrl.CreateGame(gameConf.PlayMode, gameConf.Difficulty, gameConf.LifeCnt, gameConf.StageSizeX, gameConf.StageSizeZ, gameConf.ObstacleRate, gameConf.VolcanoCnt);

        return gameCtrl;
    }

    public void UpdateProgress(Difficulty difficulty)
    {
        GameProgress[difficulty] = true;
    }

    public void SetUIText()
    {
        Text[] textCompArray = FindObjectsOfType<Text>(true);

        foreach (Text textComp in textCompArray)
        {
            UITextSet UITextSet = uiTextDic[SceneManager.GetActiveScene().name][textComp.gameObject.name][gameConf.Lang];

            textComp.text = UITextSet.Text;
            textComp.font = UITextSet.Font;
        }
    }

    void GetUITextDic()
    {
        string sceneNm = "Title";
        uiTextDic[sceneNm] = new Dictionary<string, Dictionary<Language, UITextSet>>();
        uiTextDic[sceneNm]["TitleText"] = UITextSet.CreateLangSet("コンビニ\r\n行く", "I'M GOING SHOPPING", "편의점\r\n간다", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["LangBtn"] = UITextSet.CreateLangSet("LANGUAGE", "LANGUAGE", "LANGUAGE", enDefaultFont, enDefaultFont, enDefaultFont);
        uiTextDic[sceneNm]["Ja"] = UITextSet.CreateLangSet("日本語", "日本語", "日本語", jaDefaultFont, jaDefaultFont, jaDefaultFont);
        uiTextDic[sceneNm]["En"] = UITextSet.CreateLangSet("ENGLISH", "ENGLISH", "ENGLISH", enDefaultFont, enDefaultFont, enDefaultFont);
        uiTextDic[sceneNm]["Ko"] = UITextSet.CreateLangSet("한국어", "한국어", "한국어", koDefaultFont, koDefaultFont, koDefaultFont);
        uiTextDic[sceneNm]["StartBtn"] = UITextSet.CreateLangSet("スタート", "START", "시작하기", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["BeginnerBtn"] = UITextSet.CreateLangSet("はじめて", "BEGINNER", "처음", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["EasyBtn"] = UITextSet.CreateLangSet("かんたん", "EASY", "쉬움", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["NormalBtn"] = UITextSet.CreateLangSet("ふつう", "NORMAL", "보통", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["HardBtn"] = UITextSet.CreateLangSet("むずかしい", "HARD", "어려움", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["BackBtn"] = UITextSet.CreateLangSet("<- 戻る", "<- BACK", "<- 돌아가기", jaFont, enFont, koFont);

        sceneNm = "Prologue";
        uiTextDic[sceneNm] = new Dictionary<string, Dictionary<Language, UITextSet>>();
        uiTextDic[sceneNm]["PrologueText"] = UITextSet.CreateLangSet("プロローグ", "PROLOGUE", "프롤로그", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["Page1TopTitleText"] = UITextSet.CreateLangSet("このゲームのひとたち", "CHARACTERS", "게임에 나오는 사람들", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["PlayerNameText"] = UITextSet.CreateLangSet("ひより", "Hiyori", "히요리", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["PlayerDescText"] = UITextSet.CreateLangSet("小学校1年生。女の子。\r\nお菓子が大好き。\r\nコンビニが好き。\r\n遠いけど（2時間）。。", "First grade in elementary school. Cute girl\r\nLoves sweets\r\nLoves convenience store", "초등학교 1학년. 여자아이\r\n과자가 좋아\r\n편의점이 좋아\r\n(2시간 걸리지만..)", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["ConveniNameText"] = UITextSet.CreateLangSet("コンビニ", "Convenience store", "편의점", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["ConveniDescText"] = UITextSet.CreateLangSet("お菓子が多い！\r\nワクワクする！", "A lot of sweets\r\nPit-a-pat", "과자가 많아!\r\n두근♡두근♡", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["AnimalRunNameText"] = UITextSet.CreateLangSet("牛", "Cow", "소", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["AnimalRunDescText"] = UITextSet.CreateLangSet("森に住む野生動物。\r\n体が大きくて良く走る。\r\n得意は頭突つき。", "Wild animal in the forest\r\nBig and fast\r\nLoves running", "숲에 사는 야생동물\r\n크고 잘달려\r\n박치기 선수", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["AnimalFlyNameText"] = UITextSet.CreateLangSet("トビ", "Kite", "솔개", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["AnimalFlyDescText"] = UITextSet.CreateLangSet("空を飛ぶ動物。\r\nどこで寝るか分からない。\r\nパンを奪いに襲って来る。", "Flying animal\r\nLoves blue sky", "하늘을 나는 동물\r\n어디서 자는 걸까?\r\n빵 뺏아가는 애", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["ComeUpBlockNameText"] = UITextSet.CreateLangSet("ジャンプキューブ", "Jump cube", "점프 상자", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["ComeUpBlockDescText"] = UITextSet.CreateLangSet("踏む人を飛ばすキューブ。\r\nやってみると割と面白い。", "It jumps when stepped on\r\nIt is more fun than trampolin", "밟으면 점프하는 상자\r\n트램플린보다 재밌어", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["FallDownBlockNameText"] = UITextSet.CreateLangSet("落下キューブ", "Falling cube", "낙하 상자", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["FallDownBlockDescText"] = UITextSet.CreateLangSet("踏むと落下するキューブ。\r\n乗ると危険だけど、\r\n底が見たい時は。。", "It falls when stepped on\r\nDangerous", "밟으면 떨어지는 상자\r\n타면 위험해\r\n근데 아래가 보여", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["AnimalRunBlockNameText"] = UITextSet.CreateLangSet("牛キューブ", "Cow cube", "소 상자", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["AnimalRunBlockDescText"] = UITextSet.CreateLangSet("踏むと牛が現れる。", "Cow appears when stepped on", "밟으면 소가 나오는 상자", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["AnimalFlyBlockNameText"] = UITextSet.CreateLangSet("トビキューブ", "Kite cube", "솔개 상자", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["AnimalFlyBlockDescText"] = UITextSet.CreateLangSet("踏むとトビが現れる。", "Kite appears when stepped on", "밟으면 솔개가 나오는 상자", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["NormalBlockNameText"] = UITextSet.CreateLangSet("森キューブ", "Forest cube", "숲 상자", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["NormalBlockDescText"] = UITextSet.CreateLangSet("草が生えている土。\r\n踏んでも安全。", "Grassy ground.\r\nJust ground", "풀 있는 땅\r\n그냥 땅", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["VolcanoBlockNameText"] = UITextSet.CreateLangSet("火山", "Volcano", "화산", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["VolcanoBlockDescText"] = UITextSet.CreateLangSet("地震を伴う爆発をする。\r\n溶岩を吹き出す。\r\n当たると。。", "It explodes and causes earthquake\r\nLava spouts", "지진을 일으키고 폭발해\r\n용암이 나와\r\n닿으면..", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["Page2ChatText"] = UITextSet.CreateLangSet("私のなまえはひより。\r\n\r\nおかしがたべたい。", "I am Hiyori\r\nI'd like to eat sweets", "내 이름은 히요리\r\n과자가 먹고 싶어", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["Page3ChatText"] = UITextSet.CreateLangSet("コンビニ行く。", "I'M GOING SHOPPING", "편의점 간다", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["ArrowControlText"] = UITextSet.CreateLangSet("いどう　", "MOVE", "이동", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["SpaceText"] = UITextSet.CreateLangSet("スペース", "SPACE", "스페이스", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["JumpControlText"] = UITextSet.CreateLangSet("ジャンプ", "JUMP", "점프", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["SkipBtn"] = UITextSet.CreateLangSet("スキップ", "SKIP", "건너뛰기", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["NextBtn"] = UITextSet.CreateLangSet("次へ ->", "NEXT ->", "다음 ->", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["StartBtn"] = UITextSet.CreateLangSet("スタート", "START", "시작", jaFont, enFont, koFont);

        sceneNm = "Game";
        uiTextDic[sceneNm] = new Dictionary<string, Dictionary<Language, UITextSet>>();
        uiTextDic[sceneNm]["ActionCamNameText"] = UITextSet.CreateLangSet("アクション", "ACTION", "액션", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["TopCamNameText"] = UITextSet.CreateLangSet("トップ", "TOP", "톱", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["ContinueText"] = UITextSet.CreateLangSet("コンティニュー？", "CONTINUE ?", "계속할래?", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["SuccessText"] = UITextSet.CreateLangSet("おめでとう", "CONGRATULATIONS", "축하해", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["CountText"] = UITextSet.CreateLangSet("10", "10", "10", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["ContinueBtn"] = UITextSet.CreateLangSet("もう一回", "CONTINUE", "계속할래", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["RestartBtn"] = UITextSet.CreateLangSet("リトライ", " RESTART ", "처음부터", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["TitleBtn"] = UITextSet.CreateLangSet("タイトル", "TO TITLE", "타이틀로", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["EpilogueBtn"] = UITextSet.CreateLangSet("エピローグ ->", "EPILOGUE ->", "에필로그 ->", jaFont, enFont, koFont);

        sceneNm = "Epilogue";
        uiTextDic[sceneNm] = new Dictionary<string, Dictionary<Language, UITextSet>>();
        uiTextDic[sceneNm]["EpilogueText"] = UITextSet.CreateLangSet("エピローグ", "EPILOGUE", "에필로그", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["Page1ChatText"] = UITextSet.CreateLangSet("お菓子おいしい。", "YUMMY", "과자 맛있어", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["Page2ChatText"] = UITextSet.CreateLangSet("今度一緒に行こう。", "Let's come together", "다음에 같이 오자", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["GameOverText"] = UITextSet.CreateLangSet("ゲームオーバー", "GAME OVER", "게임 오버", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["ThankyouText"] = UITextSet.CreateLangSet("サンキューフォープレイング", "THANK YOU FOR PLAYING", "고마워", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["SkipBtn"] = UITextSet.CreateLangSet("スキップ", "SKIP", "건너뛰기", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["NextBtn"] = UITextSet.CreateLangSet("次へ ->", "NEXT ->", "다음 ->", jaFont, enFont, koFont);
        uiTextDic[sceneNm]["TitleBtn"] = UITextSet.CreateLangSet("タイトルへ", "TO TITLE", "처음화면으로", jaFont, enFont, koFont);
    }
}
