using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Language
{
    JA,
    EN,
    KO
}

public enum PlayMode
{
    Intro,
    Play
}

public enum Difficulty
{
    Beginner,
    Easy,
    Normal,
    Hard,
    Custom
}

public enum ActionCamType
{
    ACTION,
    TOP
}