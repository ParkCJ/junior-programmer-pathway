﻿using UnityEngine;

public class GameConfig
{
    public static readonly Vector3 IntroCamPosition = new Vector3(7, 11.5f, -8);
    public static readonly Vector3 IntroCamRotation = new Vector3(40, 0, 0);
    public static readonly Vector3 BeginnerCamPosition = new Vector3(2, 4f, -6f);
    public static readonly Vector3 BeginnerCamRotation = new Vector3(20f, 0, 0);
    public static readonly Vector3 EasyCamPosition = new Vector3(2, 6f, -7f);
    public static readonly Vector3 EasyCamRotation = new Vector3(30f, 0, 0);
    public static readonly Vector3 NormalCamPosition = new Vector3(4.5f, 7.5f, -5f);
    public static readonly Vector3 NormalCamRotation = new Vector3(40f, 0, 0);
    public static readonly Vector3 HardCamPosition = new Vector3(7, 9.5f, -6);
    public static readonly Vector3 HardCamRotation = new Vector3(40, 0, 0);
    public static readonly Vector3 PlayerDefPosition = new Vector3(0, 1.5f, 0);
    public static readonly Vector3 StartBlockDefPosition = new Vector3(0, 0, 0);
    public const int Block1x1Height = 1;
    public const int MaxBlockLength = 5;
    public const float StageRotationSpeed = -5f;

    readonly PlayMode defPlayMode = PlayMode.Play;
    readonly Difficulty defDifficulty = Difficulty.Normal;
    readonly Language defLang = Language.EN;
    const int defLifeCnt = 3;
    const int defStageSizeX = 10;
    const int defStageSizeZ = 10;
    const float defObstacleRate = 0.5f;
    const int defVolcanoCnt = 0;

    const int beginnerLifeCnt = 5;
    const int beginnerStageSizeX = 5;
    const int beginnerStageSizeZ = 5;
    const float beginnerObstacleRate = 0.1f;
    const int beginnerVolcanoCnt = 0;

    const int easyLifeCnt = 5;
    const int easyStageSizeX = 5;
    const int easyStageSizeZ = 5;
    const float easyObstacleRate = 0.3f;
    const int easyVolcanoCnt = 0;

    const int normalLifeCnt = 3;
    const int normalStageSizeX = 10;
    const int normalStageSizeZ = 10;
    const float normalObstacleRate = 0.5f;
    const int normalVolcanoCnt = 0;

    const int hardLifeCnt = 3;
    const int hardStageSizeX = 15;
    const int hardStageSizeZ = 15;
    const float hardObstacleRate = 0.7f;
    const int hardVolcanoCnt = 1;

    public PlayMode PlayMode { get; private set; }
    public Difficulty Difficulty { get; private set; }
    public Language Lang { get; private set; }
    public int LifeCnt { get; private set; }
    public int StageSizeX { get; private set; }
    public int StageSizeZ { get; private set; }
    public float ObstacleRate { get; private set; }
    public int VolcanoCnt { get; private set; }

    public GameConfig()
    {
        ChangeToDefault();
    }

    public void SetConfig(Difficulty difficulty, PlayMode playMode)
    {
        switch (difficulty)
        {
            case Difficulty.Beginner:
                PlayMode = playMode;
                Difficulty = Difficulty.Beginner;
                LifeCnt = beginnerLifeCnt;
                StageSizeX = beginnerStageSizeX;
                StageSizeZ = beginnerStageSizeZ;
                ObstacleRate = beginnerObstacleRate;
                VolcanoCnt = beginnerVolcanoCnt;
                break;
            case Difficulty.Easy:
                PlayMode = playMode;
                Difficulty = Difficulty.Easy;
                LifeCnt = easyLifeCnt;
                StageSizeX = easyStageSizeX;
                StageSizeZ = easyStageSizeZ;
                ObstacleRate = easyObstacleRate;
                VolcanoCnt = easyVolcanoCnt;
                break;
            case Difficulty.Normal:
                PlayMode = playMode;
                Difficulty = Difficulty.Normal;
                LifeCnt = normalLifeCnt;
                StageSizeX = normalStageSizeX;
                StageSizeZ = normalStageSizeZ;
                ObstacleRate = normalObstacleRate;
                VolcanoCnt = normalVolcanoCnt;
                break;
            case Difficulty.Hard:
                PlayMode = playMode;
                Difficulty = Difficulty.Hard;
                LifeCnt = hardLifeCnt;
                StageSizeX = hardStageSizeX;
                StageSizeZ = hardStageSizeZ;
                ObstacleRate = hardObstacleRate;
                VolcanoCnt = hardVolcanoCnt;
                break;
            default:
                break;
        }
    }

    public void SetConfig(Language lang)
    {
        this.Lang = lang;
    }

    public void ChangeToDefault()
    {
        PlayMode = defPlayMode;
        Difficulty = defDifficulty;
        Lang = defLang;
        LifeCnt = defLifeCnt;
        StageSizeX = defStageSizeX;
        StageSizeZ = defStageSizeZ;
        ObstacleRate = defObstacleRate;
        VolcanoCnt = defVolcanoCnt;
    }
}
