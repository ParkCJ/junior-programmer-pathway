﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UITextSet
{
    public string Text { get; set; }
    public Font Font { get; set; }

    public UITextSet(string text, Font font)
    {
        this.Text = text;
        this.Font = font;
    }

    public static Dictionary<Language, UITextSet> CreateLangSet(string jaText, string enText, string koText, Font jaFont, Font enFont, Font koFont)
    {
        Dictionary<Language, UITextSet> langSet = new Dictionary<Language, UITextSet>();
        langSet[Language.JA] = new UITextSet(jaText, jaFont);
        langSet[Language.EN] = new UITextSet(enText, enFont);
        langSet[Language.KO] = new UITextSet(koText, koFont);

        return langSet;
    }
}