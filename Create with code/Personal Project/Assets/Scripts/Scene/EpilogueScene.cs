using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EpilogueScene : MonoBehaviour
{
    const float bgImgMoveSpeed = 5f;

    enum Page
    {
        Page1,
        Page2,
        Page3
    }

    [Header("Character")]
    [SerializeField] GameObject playerGameClear;
    Animator playerGameClearAnim;

    [SerializeField] GameObject player;
    Animator playerAnim;

    [SerializeField] GameObject animalRun;
    Animator animalRunAnim;

    [SerializeField] GameObject animalFly;
    Animator animalFlyAnim;

    [Header("Camera")]
    [SerializeField] Camera goalBlockCam;
    [SerializeField] Camera gameClearCam;

    [Header("UI")]
    [SerializeField] Image bgImg;
    [SerializeField] GameObject page1;
    [SerializeField] GameObject page2;
    [SerializeField] GameObject page3;
    [SerializeField] Button skipBtn;
    [SerializeField] Button nextBtn;
    [SerializeField] Button titleBtn;
    [SerializeField] RawImage page1CamImg;
    [SerializeField] RawImage page2CamImg;
    [SerializeField] RawImage page3CamImg;

    [Header("Audio")]
    [SerializeField] AudioClip btnClickSound;

    GameManager gameManager;
    Vector3 bgImgDefPos;
    Page currentPage = Page.Page1;
    RenderTexture goalBlockCamRndTexture;
    RenderTexture gameClearCamRndTexture;
    AudioSource cameraAudio;

    float bgImgHalfWidth;

    void Awake()
    {
        // GameManager
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();

        // Player
        playerGameClearAnim = playerGameClear.transform.Find("Girl").GetComponent<Animator>();
        playerAnim = player.transform.Find("Girl").GetComponent<Animator>();
        animalRunAnim = animalRun.transform.Find("Animal").GetComponent<Animator>();
        cameraAudio = Camera.main.GetComponent<AudioSource>();

        // UI
        bgImgDefPos = bgImg.transform.position;
        bgImgHalfWidth = bgImg.rectTransform.rect.width / 2;

        skipBtn.onClick.AddListener(() =>
        {
            PlayBtnClickSound();

            MoveToLastPage();
        });

        nextBtn.onClick.AddListener(() =>
        {
            PlayBtnClickSound();

            ToNextPage();
        });

        titleBtn.onClick.AddListener(() =>
        {
            PlayBtnClickSound();

            ToTitle();
        });

        // RenderTexture
        SetCamera(goalBlockCam, goalBlockCamRndTexture, page1CamImg, page2CamImg);
        SetCamera(gameClearCam, gameClearCamRndTexture, page3CamImg);
    }

    void Start()
    {
        gameManager.SetUIText();

        playerGameClearAnim.SetBool("WipeMouth", true);
        playerAnim.SetFloat("Speed_f", 1);
        animalRunAnim.SetFloat("Speed_f", 1f);
    }

    void Update()
    {
        if (bgImg.rectTransform.offsetMin.x < -bgImgHalfWidth)
            bgImg.transform.position = bgImgDefPos;
        else
            bgImg.transform.Translate(Vector3.left * bgImgMoveSpeed * Time.deltaTime);
    }

    void PlayBtnClickSound()
    {
        AudioSource.PlayClipAtPoint(btnClickSound, Camera.main.transform.position, 0.15f);
    }

    void SetCamera(Camera camera, RenderTexture rndTexture, params RawImage[] cameraImgs)
    {
        rndTexture = new RenderTexture(256, 256, 24);
        rndTexture.name = System.Guid.NewGuid().ToString();
        rndTexture.Create();
        camera.targetTexture = rndTexture;

        foreach(RawImage img in cameraImgs)
            img.texture = rndTexture;
    }

    void MoveToLastPage()
    {
        page1.SetActive(false);
        page2.SetActive(false);
        page3.SetActive(true);
        skipBtn.gameObject.SetActive(false);
        nextBtn.gameObject.SetActive(false);
        titleBtn.gameObject.SetActive(true);
        currentPage = Page.Page3;

        cameraAudio.Play();
    }

    void ToNextPage()
    {
        switch(currentPage)
        {
            case Page.Page1:
                page1.SetActive(false);
                page2.SetActive(true);

                // Dance, HandOnHips, SitDown, CrossArms
                playerGameClearAnim.SetBool("WipeMouth", false);
                playerGameClearAnim.SetBool("CrossArms", true);

                currentPage = Page.Page2;
                break;
            case Page.Page2:
                MoveToLastPage();
                break;
            default:
                break;
        }
    }

    void ToTitle()
    {
        SceneManager.LoadScene("Title");
    }

    void OnDestroy()
    {
        if (goalBlockCamRndTexture != null)
            goalBlockCamRndTexture.Release();

        if (gameClearCamRndTexture != null)
            gameClearCamRndTexture.Release();
    }
}