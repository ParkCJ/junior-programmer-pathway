using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class GameScene : MonoBehaviour
{
    [Header("UI")]
    [SerializeField] Canvas uiCanvas;
    [SerializeField] GridLayoutGroup lifeUI;
    [SerializeField] GameObject heartUIPrefab;
    [SerializeField] GameObject secondCamArea;
    [SerializeField] RawImage secondCamImg;
    [SerializeField] Button camChangeBtn;
    [SerializeField] Text actionCamNameText;
    [SerializeField] Text topCamNameText;
    [SerializeField] Text continueText;
    [SerializeField] Text successText;
    [SerializeField] Text countText;
    [SerializeField] Button continueBtn;
    [SerializeField] Button restartBtn;
    [SerializeField] Button titleBtn;
    [SerializeField] Button epilogueBtn;

    [Header("Audio")]
    [SerializeField] AudioClip btnClickSound;

    GameManager gameManager;
    GameController gameCtrl;
    Coroutine gameOverCountdownCoroutine;

    float cloudSpeed;
    int toTitleCountDown = 10;

    void Awake()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        cloudSpeed = Random.Range(-2f, 2f);
    }

    void Start()
    {
        gameManager.SetUIText();

        StartCoroutine(StartGame());
    }

    void Update()
    {
        RenderSettings.skybox.SetFloat("_Rotation", Time.time * cloudSpeed);
    }

    IEnumerator StartGame()
    {
        // UI button events
        continueBtn.onClick.AddListener(() =>
        {
            PlayBtnClickSound();

            HideGameOver();
            gameCtrl.ContinueGame();
            StartCoroutine(UpdateUI());
        });

        restartBtn.onClick.AddListener(() =>
        {
            PlayBtnClickSound();

            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        });

        titleBtn.onClick.AddListener(() =>
        {
            PlayBtnClickSound();

            SceneManager.LoadScene("Title");
        });

        epilogueBtn.onClick.AddListener(() =>
        {
            PlayBtnClickSound();

            SceneManager.LoadScene("Epilogue");
        });

        // Create game
        gameCtrl = gameManager.CreateGame();

        while (!gameCtrl.GameLoaded)
        {
            yield return new WaitForEndOfFrame();
        }

        // After game load
        uiCanvas.gameObject.SetActive(true);

        if (gameCtrl.ActionCamRndTexture == null)
            secondCamArea.gameObject.SetActive(false);
        else
        {
            secondCamArea.gameObject.SetActive(true);
            secondCamImg.texture = gameCtrl.ActionCamRndTexture;
            actionCamNameText.gameObject.SetActive(true);
            topCamNameText.gameObject.SetActive(false);

            camChangeBtn.onClick.AddListener(() =>
            {
                PlayBtnClickSound();

                if (gameCtrl.ActionCamType == ActionCamType.ACTION)
                {
                    gameCtrl.SetSecCamAngle(ActionCamType.TOP);
                    actionCamNameText.gameObject.SetActive(false);
                    topCamNameText.gameObject.SetActive(true);
                }
                else
                {
                    gameCtrl.SetSecCamAngle(ActionCamType.ACTION);
                    actionCamNameText.gameObject.SetActive(true);
                    topCamNameText.gameObject.SetActive(false);
                }

                EventSystem.current.SetSelectedGameObject(null);
            });
        }

        StartCoroutine(UpdateUI());
    }

    IEnumerator UpdateUI()
    {
        for (int i = 0; i < gameCtrl.LifeCnt; i++)
            Instantiate(heartUIPrefab, lifeUI.transform);

        while (gameCtrl.GamePlaying)
        {
            yield return new WaitForEndOfFrame();

            if (lifeUI.gameObject.transform.childCount > gameCtrl.LifeCnt)
            {
                Destroy(lifeUI.gameObject.transform.GetChild(0).gameObject);

                if (gameCtrl.LifeCnt == 0)
                    ShowGameOver();
            }
        }

        if (gameCtrl.GameCleared)
            ShowGameClear();
    }

    void ShowGameOver()
    {
        continueText.gameObject.SetActive(true);
        continueBtn.gameObject.SetActive(true);
        restartBtn.gameObject.SetActive(true);
        titleBtn.gameObject.SetActive(true);
        countText.gameObject.SetActive(true);

        gameOverCountdownCoroutine = StartCoroutine(GameOverCountdown());
    }

    IEnumerator GameOverCountdown()
    {
        int timeLeft = toTitleCountDown;

        while (timeLeft >= 0)
        {
            countText.text = timeLeft.ToString();
            timeLeft--;

            yield return new WaitForSeconds(1);
        }

        SceneManager.LoadScene("Title");
    }

    void HideGameOver()
    {
        continueText.gameObject.SetActive(false);
        continueBtn.gameObject.SetActive(false);
        restartBtn.gameObject.SetActive(false);
        titleBtn.gameObject.SetActive(false);
        countText.gameObject.SetActive(false);

        StopCoroutine(gameOverCountdownCoroutine);
    }

    void ShowGameClear()
    {
        successText.gameObject.SetActive(true);
        epilogueBtn.gameObject.SetActive(true);
    }

    void PlayBtnClickSound()
    {
        AudioSource.PlayClipAtPoint(btnClickSound, Camera.main.transform.position, 0.15f);
    }
}
