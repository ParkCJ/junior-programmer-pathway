using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PrologueScene : MonoBehaviour
{
    const float blockSpawnIntervalMin = 0.9f;
    const float blockSpawnIntervalMax = 1.2f;
    const float bgImgMoveSpeed = 5f;

    enum Page
    {
        Page1,
        Page2,
        Page3,
        Page4
    }

    [Header("Character")]
    [SerializeField] GameObject player;
    Rigidbody playerRb;
    Animator playerAnim;

    [SerializeField] GameObject playerToControl;
    Rigidbody playerToControlRb;
    Animator playerToControlAnim;

    [SerializeField] GameObject animalRun;
    Rigidbody animalRunRb;
    Animator animalRunAnim;

    [SerializeField] GameObject animalFly;
    Rigidbody animFlyRb;
    Animator animalFlyAnim;

    [Header("Camera")]
    [SerializeField] Camera playerCam;
    [SerializeField] Camera goalBlockCam;
    [SerializeField] Camera animalRunCam;
    [SerializeField] Camera animalFlyCam;
    [SerializeField] Camera comeUpBlockCam;
    [SerializeField] Camera fallDownBlockCam;
    [SerializeField] Camera animalRunBlockCam;
    [SerializeField] Camera animalFlyBlockCam;
    [SerializeField] Camera normalBlockCam;
    [SerializeField] Camera volcanoBlockCam;
    [SerializeField] Camera playerControlCam;

    [Header("Block")]
    [SerializeField] GameObject animalFlyBlockPlane;
    [SerializeField] GameObject animalFlyBlockCube;
    [SerializeField] GameObject animalFlyBlockAnimal;

    [SerializeField] GameObject animalRunBlockPlane;
    [SerializeField] GameObject animalRunBlockCube;
    [SerializeField] GameObject animalRunBlockAnimal;

    [SerializeField] GameObject comeUpBlock;
    [SerializeField] GameObject comeUpBlockPlane;
    [SerializeField] GameObject comeUpBlockCube;
    Vector3 comeUpBlockCubePos;

    [SerializeField] GameObject fallDownBlock;
    [SerializeField] GameObject fallDownBlockPlane;
    [SerializeField] GameObject fallDownBlockCube;
    Vector3 fallDownBlockCubePos;

    [SerializeField] GameObject volcanoBlock;
    [SerializeField] GameObject lavaBlock;
    GameObject volcanoGroundLev1;
    GameObject volcanoGroundLev2;
    GameObject volcanoGroundLev3;
    GameObject volcanoGroundTop;
    GameObject volcanoTopFlame;
    List<GameObject> spoutOutLavaList;

    [Header("Materials")]
    [SerializeField] Material sandMaterial;
    [SerializeField] Material sand5x5Material;
    [SerializeField] Material grassMaterial;
    [SerializeField] Material rockMaterial;
    [SerializeField] Material volcanoMaterial;
    [SerializeField] Material animalFlyBlockMaterial;
    [SerializeField] Material animalRunBlockMaterial;
    [SerializeField] Material comeUpBlockMaterial;
    [SerializeField] Material fallDownBlockMaterial;
    [SerializeField] Material volcanoBlockMaterial;

    [Header("UI")]
    [SerializeField] Image bgImg;
    [SerializeField] GameObject page1;
    [SerializeField] GameObject page2;
    [SerializeField] GameObject page3;
    [SerializeField] GameObject page4;
    [SerializeField] Button skipBtn;
    [SerializeField] Button nextBtn;
    [SerializeField] Button startBtn;
    [SerializeField] RawImage playerCamImg;
    [SerializeField] RawImage goalBlockCamImg;
    [SerializeField] RawImage animalRunCamImg;
    [SerializeField] RawImage animalFlyCamImg;
    [SerializeField] RawImage comeUpBlockCamImg;
    [SerializeField] RawImage fallDownBlockCamImg;
    [SerializeField] RawImage animalRunBlockCamImg;
    [SerializeField] RawImage animalFlyBlockCamImg;
    [SerializeField] RawImage normalBlockCamImg;
    [SerializeField] RawImage volcanoBlockCamImg;
    [SerializeField] RawImage page2PlayerCamImg;
    [SerializeField] RawImage page3PlayerCamImg;
    [SerializeField] RawImage playerControlCamImg;

    [Header("Audio")]
    [SerializeField] AudioClip btnClickSound;

    GameManager gameManager;
    Vector3 bgImgDefPos;
    Page currentPage = Page.Page1;
    RenderTexture playerCamRndTexture;
    RenderTexture goalBlockCamRndTexture;
    RenderTexture animalRunCamRndTexture;
    RenderTexture animalFlyCamRndTexture;
    RenderTexture comeUpBlockCamRndTexture;
    RenderTexture fallDownBlockCamRndTexture;
    RenderTexture animalRunBlockCamRndTexture;
    RenderTexture animalFlyBlockCamRndTexture;
    RenderTexture normalBlockCamRndTexture;
    RenderTexture volcanoBlockCamRndTexture;
    RenderTexture playerControlCamRndTexture;

    float bgImgHalfWidth;

    void Awake()
    {
        // Player
        playerRb = player.GetComponent<Rigidbody>();
        playerAnim = player.transform.Find("Girl").GetComponent<Animator>();

        // PlayerToControl
        playerToControlRb = playerToControl.GetComponent<Rigidbody>();
        playerToControlAnim = playerToControl.transform.Find("Girl").GetComponent<Animator>();

        // AnimalRun
        animalRunRb = animalRun.GetComponent<Rigidbody>();
        animalRunAnim = animalRun.transform.Find("Animal").GetComponent<Animator>();

        // AnimalFly
        animFlyRb = animalFly.GetComponent<Rigidbody>();
        animalFlyAnim = animalFly.transform.Find("Animal").GetComponent<Animator>();

        // GameManager
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();

        // UI
        bgImgDefPos = bgImg.transform.position;
        bgImgHalfWidth = bgImg.rectTransform.rect.width / 2;

        skipBtn.onClick.AddListener(() =>
        {
            PlayBtnClickSound();

            SkipPrologue();
        });

        nextBtn.onClick.AddListener(() =>
        {
            PlayBtnClickSound();

            ToNextPage();
        });

        startBtn.onClick.AddListener(() =>
        {
            PlayBtnClickSound();

            StartGame();
        });

        // RenderTexture
        SetCamera(playerCam, playerCamRndTexture, playerCamImg, page2PlayerCamImg, page3PlayerCamImg);
        SetCamera(goalBlockCam, goalBlockCamRndTexture, goalBlockCamImg);
        SetCamera(animalRunCam, animalRunCamRndTexture, animalRunCamImg);
        SetCamera(animalFlyCam, animalFlyCamRndTexture, animalFlyCamImg);
        SetCamera(comeUpBlockCam, comeUpBlockCamRndTexture, comeUpBlockCamImg);
        SetCamera(fallDownBlockCam, fallDownBlockCamRndTexture, fallDownBlockCamImg);
        SetCamera(animalRunBlockCam, animalRunBlockCamRndTexture, animalRunBlockCamImg);
        SetCamera(animalFlyBlockCam, animalFlyBlockCamRndTexture, animalFlyBlockCamImg);
        SetCamera(normalBlockCam, normalBlockCamRndTexture, normalBlockCamImg);
        SetCamera(volcanoBlockCam, volcanoBlockCamRndTexture, volcanoBlockCamImg);
        SetCamera(playerControlCam, playerControlCamRndTexture, playerControlCamImg);
    }

    void PlayBtnClickSound()
    {
        AudioSource.PlayClipAtPoint(btnClickSound, Camera.main.transform.position, 0.15f);
    }

    void SetCamera(Camera camera, RenderTexture rndTexture, params RawImage[] cameraImgs)
    {
        rndTexture = new RenderTexture(256, 256, 24);
        rndTexture.name = System.Guid.NewGuid().ToString();
        rndTexture.Create();
        camera.targetTexture = rndTexture;

        foreach (RawImage img in cameraImgs)
            img.texture = rndTexture;
    }

    void Start()
    {
        gameManager.SetUIText();

        // Character
        PlayerAction();
        AnimalRunAction();
        AnimalFlyAction();

        // Block
        StartCoroutine(AnimalFlyBlock_Step1());
        StartCoroutine(AnimalRunBlock_Step1());

        comeUpBlockCubePos = comeUpBlockCube.transform.position;
        StartCoroutine(ComeUpBlock_Step1());

        fallDownBlockCubePos = fallDownBlockCube.transform.position;
        StartCoroutine(FallDownBlock_Step1());

        volcanoGroundLev1 = volcanoBlock.transform.Find("GroundLev1").gameObject;
        volcanoGroundLev2 = volcanoBlock.transform.Find("GroundLev2").gameObject;
        volcanoGroundLev3 = volcanoBlock.transform.Find("GroundLev3").gameObject;
        volcanoGroundTop = volcanoBlock.transform.Find("GroundTop").gameObject;
        volcanoTopFlame = volcanoGroundTop.transform.Find("TopFlame").gameObject;
        spoutOutLavaList = new List<GameObject>();
        StartCoroutine(VolcanoBlock_Step1());
    }

    void FixedUpdate()
    {
        if (currentPage == Page.Page4)
        {
            float horInput = Input.GetAxis("Horizontal");
            float vertInput = Input.GetAxis("Vertical");

            Vector3 newFoward = new Vector3(horInput, 0, vertInput).normalized;
            if (newFoward != Vector3.zero)
                playerToControl.transform.forward = newFoward;

            float speedF = 0.0f;
            bool hasHInput = Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow);
            bool hasVInput = Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.DownArrow);

            if (hasHInput && hasVInput)
                speedF = (Mathf.Abs(horInput) + Mathf.Abs(vertInput)) / 2;
            else if (hasHInput)
                speedF = Mathf.Abs(horInput);
            else if (hasVInput)
                speedF = Mathf.Abs(vertInput);
            else
                speedF = 0.0f;

            playerToControlAnim.SetFloat("Speed_f", speedF);
            playerToControl.transform.Translate(Vector3.forward * speedF * Time.deltaTime);

            // Jump
            bool spaceKeyDown = Input.GetKey(KeyCode.Space);
            bool canJump = Physics.Raycast(playerToControl.transform.position, Vector3.down, 0.464f);

            if (spaceKeyDown && canJump)
            {
                playerToControlAnim.SetBool("Jump_b", true);

                playerToControlRb.AddForce(Vector3.up * 220, ForceMode.Impulse);
                playerToControlRb.AddRelativeForce(Vector3.forward * speedF * 50, ForceMode.Impulse);
            }
            else
                playerToControlAnim.SetBool("Jump_b", false);
        }
    }

    void Update()
    {
        if (bgImg.rectTransform.offsetMin.x < -bgImgHalfWidth)
            bgImg.transform.position = bgImgDefPos;
        else
            bgImg.transform.Translate(Vector3.left * bgImgMoveSpeed * Time.deltaTime);
    }

    /*==================================================================================
     * Character
    ==================================================================================*/
    void PlayerAction()
    {
        // Dance, HandOnHips, SitDown, CrossArms
        playerAnim.SetBool("HandOnHips", true);
    }

    void AnimalRunAction()
    {
        animalRunAnim.SetFloat("Speed_f", 0);
        animalRunAnim.SetBool("Eat_b", true);
    }

    void AnimalFlyAction()
    {
    }

    /*==================================================================================
     * Block
    ==================================================================================*/
    // AnimalFlyBlock
    IEnumerator AnimalFlyBlock_Step1()
    {
        animalFlyBlockCube.GetComponent<Renderer>().material = animalFlyBlockMaterial;

        yield return new WaitForSeconds(Random.Range(blockSpawnIntervalMin, blockSpawnIntervalMax + 1));

        animalFlyBlockCube.GetComponent<Renderer>().material = grassMaterial;
        animalFlyBlockAnimal.gameObject.SetActive(true);

        StartCoroutine(AnimalFlyBlock_Step2());
    }

    IEnumerator AnimalFlyBlock_Step2()
    {
        yield return new WaitForSeconds(3);

        animalFlyBlockCube.GetComponent<Renderer>().material = sandMaterial;
        animalFlyBlockAnimal.gameObject.SetActive(false);

        StartCoroutine(AnimalFlyBlock_Step3());
    }

    IEnumerator AnimalFlyBlock_Step3()
    {
        yield return new WaitForSeconds(Random.Range(blockSpawnIntervalMin, blockSpawnIntervalMax + 1));

        StartCoroutine(AnimalFlyBlock_Step1());
    }

    // AnimalRunBlock
    IEnumerator AnimalRunBlock_Step1()
    {
        animalRunBlockCube.GetComponent<Renderer>().material = animalRunBlockMaterial;

        yield return new WaitForSeconds(Random.Range(blockSpawnIntervalMin, blockSpawnIntervalMax + 1));

        animalRunBlockCube.GetComponent<Renderer>().material = grassMaterial;

        animalRunBlockAnimal.gameObject.SetActive(true);
        Animator anim = animalRunBlockAnimal.transform.Find("Animal").GetComponent<Animator>();
        anim.SetFloat("Speed_f", 0);
        anim.SetBool("Eat_b", true);

        StartCoroutine(AnimalRunBlock_Step2());
    }

    IEnumerator AnimalRunBlock_Step2()
    {
        yield return new WaitForSeconds(3);

        animalRunBlockCube.GetComponent<Renderer>().material = sandMaterial;
        animalRunBlockAnimal.gameObject.SetActive(false);

        StartCoroutine(AnimalRunBlock_Step3());
    }

    IEnumerator AnimalRunBlock_Step3()
    {
        yield return new WaitForSeconds(Random.Range(blockSpawnIntervalMin, blockSpawnIntervalMax + 1));

        StartCoroutine(AnimalRunBlock_Step1());
    }

    // ComeUp Block
    IEnumerator ComeUpBlock_Step1()
    {
        comeUpBlockCube.GetComponent<Renderer>().material = comeUpBlockMaterial;

        yield return new WaitForSeconds(Random.Range(blockSpawnIntervalMin, blockSpawnIntervalMax + 1));

        comeUpBlockCube.GetComponent<Renderer>().material = grassMaterial;
        comeUpBlockCube.GetComponent<Rigidbody>().AddForce(Vector3.up * 10000, ForceMode.Impulse);
        comeUpBlockPlane.gameObject.SetActive(false);

        StartCoroutine(ComeUpBlock_Step2());
    }

    IEnumerator ComeUpBlock_Step2()
    {
        yield return new WaitForSeconds(1);

        Rigidbody rb = comeUpBlockCube.GetComponent<Rigidbody>();
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;

        comeUpBlockCube.gameObject.SetActive(false);
        comeUpBlockCube.transform.position = comeUpBlockCubePos;

        StartCoroutine(ComeUpBlock_Step3());
    }

    IEnumerator ComeUpBlock_Step3()
    {
        yield return new WaitForSeconds(2);

        comeUpBlockCube.GetComponent<Renderer>().material = sandMaterial;
        comeUpBlockCube.gameObject.SetActive(true);
        comeUpBlockPlane.gameObject.SetActive(true);

        StartCoroutine(ComeUpBlock_Step4());
    }

    IEnumerator ComeUpBlock_Step4()
    {
        yield return new WaitForSeconds(Random.Range(blockSpawnIntervalMin, blockSpawnIntervalMax + 1));

        StartCoroutine(ComeUpBlock_Step1());
    }

    // FallDown Block
    IEnumerator FallDownBlock_Step1()
    {
        fallDownBlockCube.GetComponent<Renderer>().material = fallDownBlockMaterial;

        yield return new WaitForSeconds(Random.Range(blockSpawnIntervalMin, blockSpawnIntervalMax + 1));

        fallDownBlockCube.GetComponent<Renderer>().material = rockMaterial;
        fallDownBlockCube.transform.localScale *= 0.9f;
        fallDownBlockPlane.gameObject.SetActive(false);

        StartCoroutine(FallDownBlock_Step2());
    }

    IEnumerator FallDownBlock_Step2()
    {
        yield return new WaitForSeconds(1);

        fallDownBlockCube.transform.localScale = Vector3.one;
        fallDownBlockCube.gameObject.SetActive(false);
        fallDownBlockCube.transform.position = fallDownBlockCubePos;

        StartCoroutine(FallDownBlock_Step3());
    }

    IEnumerator FallDownBlock_Step3()
    {
        yield return new WaitForSeconds(2);

        fallDownBlockCube.GetComponent<Renderer>().material = sandMaterial;
        fallDownBlockCube.gameObject.SetActive(true);
        fallDownBlockPlane.gameObject.SetActive(true);

        StartCoroutine(FallDownBlock_Step4());
    }

    IEnumerator FallDownBlock_Step4()
    {
        yield return new WaitForSeconds(Random.Range(blockSpawnIntervalMin, blockSpawnIntervalMax + 1));

        StartCoroutine(FallDownBlock_Step1());
    }

    // Volcano Block
    IEnumerator VolcanoBlock_Step1()
    {
        volcanoGroundLev1.GetComponent<Renderer>().material = volcanoBlockMaterial;

        yield return new WaitForSeconds(Random.Range(blockSpawnIntervalMin, blockSpawnIntervalMax + 1));

        StartCoroutine(VolcanoBlock_Step2());
    }

    IEnumerator VolcanoBlock_Step2()
    {
        volcanoGroundLev1.GetComponent<Renderer>().material = volcanoMaterial;

        while (!volcanoGroundTop.activeSelf)
        {
            yield return new WaitForSeconds(0.25f);

            if (!volcanoGroundLev2.activeSelf)
                volcanoGroundLev2.gameObject.SetActive(true);
            else if (!volcanoGroundLev3.activeSelf)
                volcanoGroundLev3.gameObject.SetActive(true);
            else if (!volcanoGroundTop.activeSelf)
                volcanoGroundTop.gameObject.SetActive(true);
        }

        StartCoroutine(VolcanoBlock_Step3());
    }

    IEnumerator VolcanoBlock_Step3()
    {
        yield return new WaitForSeconds(2);

        volcanoTopFlame.SetActive(true);

        int lavaCount = 3;
        while (lavaCount > 0)
        {
            GameObject lava = Instantiate(lavaBlock, volcanoGroundTop.transform.position + Vector3.up, lavaBlock.transform.rotation);
            lava.gameObject.SetActive(true);

            spoutOutLavaList.Add(lava);

            lavaCount--;
        }

        StartCoroutine(VolcanoBlock_Step4());
    }

    IEnumerator VolcanoBlock_Step4()
    {
        int spoutOutCnt = 2;
        while (spoutOutCnt > 0)
        {
            yield return new WaitForSeconds(0.75f);

            int lavaCount = Random.Range(1, 3);
            while (lavaCount > 0)
            {
                GameObject lava = Instantiate(lavaBlock, volcanoGroundTop.transform.position + Vector3.up, lavaBlock.transform.rotation);
                lava.gameObject.SetActive(true);

                spoutOutLavaList.Add(lava);

                lavaCount--;
            }

            spoutOutCnt--;
        }

        StartCoroutine(VolcanoBlock_Step5());
    }

    IEnumerator VolcanoBlock_Step5()
    {
        yield return new WaitForSeconds(3);

        volcanoGroundLev1.GetComponent<Renderer>().material = sand5x5Material;

        volcanoGroundLev2.gameObject.SetActive(false);
        volcanoGroundLev3.gameObject.SetActive(false);
        volcanoGroundTop.gameObject.SetActive(false);
        volcanoTopFlame.gameObject.SetActive(false);

        foreach (GameObject lava in spoutOutLavaList)
            Destroy(lava);

        StartCoroutine(VolcanoBlock_Step6());
    }

    IEnumerator VolcanoBlock_Step6()
    {
        yield return new WaitForSeconds(Random.Range(blockSpawnIntervalMin, blockSpawnIntervalMax + 1));

        StartCoroutine(VolcanoBlock_Step1());
    }

    void SkipPrologue()
    {
        StartGame();
    }

    void ToNextPage()
    {
        switch(currentPage)
        {
            case Page.Page1:
                page1.SetActive(false);
                page2.SetActive(true);
                currentPage = Page.Page2;
                break;
            case Page.Page2:
                page2.SetActive(false);
                page3.SetActive(true);

                playerAnim.SetBool("HandOnHips", false);
                playerAnim.SetBool("CrossArms", true);

                currentPage = Page.Page3;
                break;
            case Page.Page3:
                page3.SetActive(false);
                page4.SetActive(true);
                skipBtn.gameObject.SetActive(false);
                nextBtn.gameObject.SetActive(false);
                startBtn.gameObject.SetActive(true);
                currentPage = Page.Page4;
                break;
            default:
                break;
        }
    }

    void StartGame()
    {
        SceneManager.LoadScene("Game");
    }

    void OnDestroy()
    {
        if (playerCamRndTexture != null)
            playerCamRndTexture.Release();

        if (goalBlockCamRndTexture != null)
            goalBlockCamRndTexture.Release();

        if (animalRunCamRndTexture != null)
            animalRunCamRndTexture.Release();

        if (animalFlyCamRndTexture != null)
            animalFlyCamRndTexture.Release();

        if (comeUpBlockCamRndTexture != null)
            comeUpBlockCamRndTexture.Release();

        if (fallDownBlockCamRndTexture != null)
            fallDownBlockCamRndTexture.Release();

        if (animalRunBlockCamRndTexture != null)
            animalRunBlockCamRndTexture.Release();

        if (animalFlyBlockCamRndTexture != null)
            animalFlyBlockCamRndTexture.Release();

        if (normalBlockCamRndTexture != null)
            normalBlockCamRndTexture.Release();

        if (volcanoBlockCamRndTexture != null)
            volcanoBlockCamRndTexture.Release();

        if (playerControlCamRndTexture != null)
            playerControlCamRndTexture.Release();
    }
}