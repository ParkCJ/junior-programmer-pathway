using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TitleScene : MonoBehaviour
{
    const float pumpInterval = 4;
    const float pumpScaleMax = 1.2f;

    [Header("UI")]
    [SerializeField] Text titleText;
    [SerializeField] Canvas uiCanvas;
    [SerializeField] Button startBtn;
    [SerializeField] GameObject difficultyArea;
    [SerializeField] Button beginnerBtn;
    [SerializeField] Button easyBtn;
    [SerializeField] Button normalBtn;
    [SerializeField] Button hardBtn;
    [SerializeField] Button backBtn;
    [SerializeField] Button langBtn;
    [SerializeField] GameObject languages;
    [SerializeField] Button jaBtn;
    [SerializeField] Button enBtn;
    [SerializeField] Button koBtn;

    [Header("Audio")]
    [SerializeField] AudioClip btnClickSound;

    GameManager gameManager;
    GameController gameCtrl;
    AudioSource cameraAudio;

    Coroutine pumpCoroutine;
    bool pump = false;

    void Awake()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        cameraAudio = Camera.main.GetComponent<AudioSource>();

        startBtn.onClick.AddListener(() =>
        {
            PlayBtnClickSound();

            startBtn.gameObject.SetActive(false);
            langBtn.gameObject.SetActive(false);
            StopCoroutine(pumpCoroutine);
            difficultyArea.gameObject.SetActive(true);
        });

        beginnerBtn.onClick.AddListener(() =>
        {
            PlayBtnClickSound();

            gameManager.ChangeConfig(Difficulty.Beginner);
            SceneManager.LoadScene("Prologue");
        });

        easyBtn.onClick.AddListener(() =>
        {
            PlayBtnClickSound();

            gameManager.ChangeConfig(Difficulty.Easy);
            SceneManager.LoadScene("Prologue");
        });

        normalBtn.onClick.AddListener(() =>
        {
            PlayBtnClickSound();

            gameManager.ChangeConfig(Difficulty.Normal);
            SceneManager.LoadScene("Prologue");
        });

        hardBtn.onClick.AddListener(() =>
        {
            PlayBtnClickSound();

            gameManager.ChangeConfig(Difficulty.Hard);
            SceneManager.LoadScene("Prologue");
        });

        backBtn.onClick.AddListener(() =>
        {
            PlayBtnClickSound();

            startBtn.transform.localScale = Vector3.one;
            startBtn.gameObject.SetActive(true);
            langBtn.gameObject.SetActive(true);
            pumpCoroutine = StartCoroutine(PumpStartBtn());
            difficultyArea.gameObject.SetActive(false);
        });

        langBtn.onClick.AddListener(() =>
        {
            PlayBtnClickSound();

            startBtn.gameObject.SetActive(false);
            langBtn.gameObject.SetActive(false);
            languages.SetActive(true);
        });

        jaBtn.onClick.AddListener(() =>
        {
            PlayBtnClickSound();

            gameManager.ChangeLanguage(Language.JA);

            startBtn.gameObject.SetActive(true);
            langBtn.gameObject.SetActive(true);
            languages.SetActive(false);
        });

        enBtn.onClick.AddListener(() =>
        {
            PlayBtnClickSound();

            gameManager.ChangeLanguage(Language.EN);

            startBtn.gameObject.SetActive(true);
            langBtn.gameObject.SetActive(true);
            languages.SetActive(false);
        });

        koBtn.onClick.AddListener(() =>
        {
            PlayBtnClickSound();

            gameManager.ChangeLanguage(Language.KO);

            startBtn.gameObject.SetActive(true);
            langBtn.gameObject.SetActive(true);
            languages.SetActive(false);
        });
    }

    void Start()
    {
        gameManager.SetUIText();

        StartCoroutine(StartGame());
    }

    IEnumerator StartGame()
    {
        // Game progress
        if (gameManager.GameProgress[Difficulty.Beginner])
            beginnerBtn.transform.Find("ClearImg").gameObject.SetActive(true);

        if (gameManager.GameProgress[Difficulty.Easy])
            easyBtn.transform.Find("ClearImg").gameObject.SetActive(true);

        if (gameManager.GameProgress[Difficulty.Normal])
            normalBtn.transform.Find("ClearImg").gameObject.SetActive(true);

        if (gameManager.GameProgress[Difficulty.Hard])
            hardBtn.transform.Find("ClearImg").gameObject.SetActive(true);

        // Create game (Watch mode)
        gameManager.ChangeConfig(Difficulty.Hard, PlayMode.Intro);
        gameCtrl = gameManager.CreateGame();

        while (!gameCtrl.GameLoaded)
        {
            yield return new WaitForEndOfFrame();
        }

        // After game load
        uiCanvas.gameObject.SetActive(true);
        cameraAudio.Play();

        pumpCoroutine = StartCoroutine(PumpStartBtn());
    }

    IEnumerator PumpStartBtn()
    {
        while(true)
        {
            yield return new WaitForSeconds(pumpInterval);

            pump = true;
        }
    }

    void PlayBtnClickSound()
    {
        AudioSource.PlayClipAtPoint(btnClickSound, Camera.main.transform.position, 0.15f);
    }

    void Update()
    {
        if (pump)
        {
            if (startBtn.transform.localScale.x < pumpScaleMax)
                startBtn.transform.localScale += Vector3.one * Time.deltaTime;
            else
                pump = false;
        }
        else
        {
            if (startBtn.transform.localScale.x > Vector3.one.x)
                startBtn.transform.localScale -= Vector3.one * Time.deltaTime;
        }
    }
}
