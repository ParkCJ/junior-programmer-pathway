﻿1. 바닥만들고, 플레이어 만들고, 적 만들고, 머티리얼의 색으로 각자를 구분한다.
	1) Primitive를 사용할 때 주의사항. 추후 진짜 모델에 따라 모양과 스케일을 선택해야 한다. (예: 사람은 스퀘어, 비행기는 스퀘어 (스케일 크게) 등)
	=> 백업을 만든다. (assets 에서 오른쪽 마우스 클릭 -> export packages)
2. PlayerController 스크립트를 만든다.
	1) Player의 움직임을 구현한다.
	2) Player의 움직임을 제한한다. (바닥 밖으로 못나가게 하거나, 나갈경우 최초위치로 리셋하게 만들거나)
	3) 신 화면의 객체들과 코드를 정리한다.
	=> 백업을 만든다.
3. Non-Player 애들을 구현한다. (작동하게 만든다. !중요! 작동하게 만들돼 미세한 것에 집착하지 않는다. 미세조정은 나중에 한다.)
	1) 모든 애들의 움직임을 만든다.
		=> 결과: 모든 애들이 움직여야 하는데로 움직인다.
	2) 모든 애들의 Destroy를 만든다. (Destory 해야 하는 경우, Destroy 되도록)
		=> 결과: 게임에서 더 이상 사용되지 않는 모든 애들은, Hierarchy에서 사라진다.
	3) Collision 과 관련된 것을 만든다.
		- Rigidbody.mass 를 수정
		- Phyical material 적용
		- Object에 태그 생성 (Collision 시, Debug.Log로 확인하기 위해)
		- OnCollisionEnter, OnTriggerEnter 작성
		=> 결과: 모든 애들이 Collision에 따라 파괴되거나, 튕기거나, 아무것도 하지 않거나 한다.
	4) Prefab 으로 만들어야 하는 애들을 모두 Prefab으로 만든다.
		=> 결과: 신에 존재하고 있을 필요가 없는 애들은 신에서 모두 삭제되어 있어야 한다.
	5) SpawnManager를 만들어서, Prefab들을 Spawn 한다.
		- Instantiate, Random.Range, InvokeRepeating 등을 사용
		=> 결과: 모든 애들이 자신의 장소에 자동으로 Spawn 되어야 한다.
4. Primitive 를 실제 모델로 바꾼다.
	1) 모델이 있는 Asset을 불러온다. (Assets/import package/custom packages)
	2) Player의 모델을 실제 모델로 바꾼다.
		PlayerPrefab의 primitive의 자식으로 실제 모델을 배치
		실제 모델의 크기, 위치 조정
		실제 모델에 Collider 추가
		그 외 필요시 수정 (Phycal material 추가 등)
		primitive의 MeshRenderer 를 체크해제
		primitive의 Collider를 체크해제
	3) Asset store에서 필요한 Asset을 가져온다. (Asset Store 폴더를 만들고 그 안에 넣는다.)
	4) Non-Player의 모델을 실제 모델로 바꾼다.
	5) Background, Ground 를 실제 Texture로 바꾼다.
		텍스처의 머티리얼의 해상도 조정 (tiling)
		텍스처의 머티리얼의 Specular highlights, reflections 체크 해제

	기타) Asset을 선택할 때 주의사항
	1) 일관성을 유지할 것. 이런 저런 스타일이 섞이면 끔직함.
	2) 심플한 것(Low poly)를 사용할 것. 고 해상도이면 게임이 느려짐.


5. 프로젝트를 최적화한다.
1) private, public, protected, readonly, const, static, [SerializeField] (Unity Editor에서 편집할 수 있게 하는 기능) 를 적절히 사용
2) awake, start, fixedUpdate, update, lateUpdate
	awake: Object가 깨어날 때 실행. 실행 후 Object의 인스턴스가 만들어진다. start 전에 실행된다. 게임 시작전 object 를 설정할 때 사용하자. script가 disabled 되어있어도 실행됨.
	start: 첫번째 Update 가 실행되기 전에 실행된다. 게임 시작의 의미. script가 enabled 되어 있어야만 실행됨.
	fixedUpdate: 움직임, 물리 연산은 여기서 처리 (Update 가 호출되기 전에 호출된다.)
	lateUpdate: 카메라 포지션 계산은 여기서 처리 (Update 종류들의 모든 연산이 끝난후, 마지막에 호출된다.)
3) 안쓰는 빈 start, update 메소드 등을 코드에서 삭제
4) ObjectPooling 을 사용 (Instantiate, Destroy 대신에, 게임시작시 Object들을 몇십개 미리 만들어 넣어두고, Active, DeActive 하면서 사용한다. 대형게임에서 성능향상)
	<Prototype 2 코드 참조>
	GameManager에 List<GameObject>를 만들고, 시작시 GameObject 10개를 Instantiate해서 (Active = false) 넣어서 참조를 만들어둠 (화면에 10개가 존재하나 안보임. List<GameObject>로 찾을 수 있음)
	이 GameObject가 필요할 때, List<GameObject> 중에서 Active = false인 애를 찾아서 Active = true 하고, Position을 설정하고 사용함
	이 GameObject가 필요없을 때, Destroy 대신에 Active = false를 실행.
	즉, 화면에 안보이게 (InActive) Object 만들어두고, 필요할 때 Active 해서 쓰고, 필요없어지면 InActive 하는 방법!

6. 트러블슈팅
7. 프로젝트 배포하기
1) 빌드하기
	[Windows]
	파일/빌드 셋팅 -> Add Open Scenes 으로 신 추가
	Player Settings 조정 (예: Windowsed, Risizable 등)
	Build
	테스트 플레이

	[WebGL]
	파일/빌드 셋팅 -> Add Open Scenes 으로 신 추가
	Player Settings 조정 (예: Windowsed, Risizable 등)
	Build and play (실행하려면 웹서버가 필요. Build and play를 하면 임시로 유니티가 웹서버 위에 띄워줌)
	Build를 저장한 폴더를 zip으로 압축
	Unity Play 또는 itch.io 에 업로드