using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    public GameObject player;
    [SerializeField] Vector3 offset = new Vector3(0, 5, -7);

    // 매 프레임마다 실행된다. 단, 실행시점이,
    // LateUpdate 는 Update가 종료된 후에 실행된다.
    private void LateUpdate()
    {
        // 자동차가와 카메라가 Update 시 움직이는데
        // 어떨때는 자동차가 먼저 움직이고, 어떨때는 카메라가 먼저 움직여서
        // Update 메소드에 아래 명령을 넣으면, 화면이 흔들린다.
        // 흔들림을 없애기 위해,
        // 자동차는 Update시 움직이고, 끝나면 카메라를 움직이게 하기 위해서 LateUpdate를 사용한다.
        transform.position = player.transform.position + offset;
    }
}
