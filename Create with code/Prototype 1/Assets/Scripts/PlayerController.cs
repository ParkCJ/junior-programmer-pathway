using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerController : MonoBehaviour
{
    [SerializeField] float horsePower;
    [SerializeField] const float turnSpeed = 45.0f;

    Rigidbody playerRb;
    [SerializeField] GameObject centerOfMass; // 무게중심점
    [SerializeField] List<WheelCollider> wheelColiders;
    [SerializeField] TextMeshProUGUI speedometerText;
    [SerializeField] TextMeshProUGUI rpmText;

    float horizontalInput;
    float verticalInput;
    float speed;
    float rpm;

    private void Start()
    {
        playerRb = GetComponent<Rigidbody>();
        playerRb.centerOfMass = centerOfMass.transform.position;
    }

    /*void FixedUpdate()
    {
        // 사용자 인풋을 받아 저장한다.
        horizontalInput = Input.GetAxis("Horizontal"); // 왼쪽 오른쪽의 값을 받는다. (-1 ~ 1). Project Setting/Input Manager/Axes에서 이름 확인 가능
        verticalInput = Input.GetAxis("Vertical"); // 앞 뒤의 값을 받는다. (-1 ~ 1). Project Setting/Input Manager/Axes에서 이름 확인 가능

        // Move the vehicle forward
        //transform.Translate(Vector3.forward); // 1 meter per frame (디바이스 성능에 따라 더 빨리 달림)
        //transform.Translate(Vector3.forward * Time.deltaTime); // 1 meter per second (시간으로 연산하고 싶을때 사용. 모든 디바이스에서 동일하게 달림)
        transform.Translate(Vector3.forward * Time.deltaTime * speed * verticalInput); // verticalInput 이 음수일때는 뒤로 움직인다.
        transform.Rotate(Vector3.up, Time.deltaTime * turnSpeed * horizontalInput); // horizontalInput 이 음수일때에는 왼쪽으로 돈다. (Y축을 기점으로)
        //transform.Translate(Vector3.right * Time.deltaTime * turnSpeed * horizontalInput); // horizontalInput 이 음수일때에는 왼쪽으로 슬라이드한다.
    }*/

    private void FixedUpdate()
    {
        horizontalInput = Input.GetAxis("Horizontal");
        verticalInput = Input.GetAxis("Vertical");

        if (IsOnGround())
        {
            playerRb.AddRelativeForce(Vector3.forward * verticalInput * horsePower);
            transform.Rotate(Vector3.up, horizontalInput * turnSpeed * Time.deltaTime);

            speed = Mathf.RoundToInt(playerRb.velocity.magnitude * 3.6f); // 2.2369362912f for 마일/h
            speedometerText.text = "Speed: " + speed + " km/h";

            rpm = Mathf.RoundToInt((speed % 30) * 40);
            rpmText.text = "RPM: " + rpm;
        }
    }

    bool IsOnGround()
    {
        int wheelsOnGround = 0;
        foreach (WheelCollider wheel in wheelColiders)
        {
            if (wheel.isGrounded)
            {
                wheelsOnGround++;
            }
        }
        if (wheelsOnGround == 4)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
