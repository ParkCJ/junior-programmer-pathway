﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerX : MonoBehaviour
{
    public GameObject dogPrefab;
    DateTime lastInstantiate;

    // Update is called once per frame
    void Update()
    {
        // On spacebar press, send dog
        if (Input.GetKeyDown(KeyCode.Space))
        {
            // 1초 이상이 지나야 다음번 Spawn을 할 수 있도록 추가
            if ((DateTime.Now - lastInstantiate).Seconds > 0)
            {
                Instantiate(dogPrefab, transform.position, dogPrefab.transform.rotation);
                lastInstantiate = DateTime.Now;
            }
        }
    }
}
