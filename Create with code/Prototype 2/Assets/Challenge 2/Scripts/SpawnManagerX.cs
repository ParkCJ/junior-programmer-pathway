﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManagerX : MonoBehaviour
{
    public GameObject[] ballPrefabs;

    private float spawnLimitXLeft = -22;
    private float spawnLimitXRight = 7;
    private float spawnPosY = 30;

    private float startDelay = 1.0f;
    private float spawnIntervalMin = 3.0f;
    private float spawnIntervalMax = 5.0f;

    // Start is called before the first frame update
    void Start()
    {
        Invoke("SpawnInRandomInterval", startDelay);
    }

    void SpawnInRandomInterval()
    {
        // 3초에서 5초사이에 랜덤하게 Spawn되도록 한다. (Coroutine을 쓰지 않고, Invoke로 처리)
        // 1. 즉시 Spawn을 하고
        // 2. 랜덤 시간 후 자신을 호출
        Invoke("SpawnRandomBall", 0);
        Invoke("SpawnInRandomInterval", Random.Range(spawnIntervalMin, spawnIntervalMax));
    }

    // Spawn random ball at random x position at top of play area
    void SpawnRandomBall ()
    {
        // Generate random ball index and random spawn position
        Vector3 spawnPos = new Vector3(Random.Range(spawnLimitXLeft, spawnLimitXRight), spawnPosY, 0);
        int ballIndex = Random.Range(0, ballPrefabs.Length);

        // instantiate ball at random spawn location
        Instantiate(ballPrefabs[ballIndex], spawnPos, ballPrefabs[ballIndex].transform.rotation);
    }

}
