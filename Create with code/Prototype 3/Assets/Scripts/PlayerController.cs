using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Rigidbody playerRb;

    // Animation
    private Animator playerAnim;

    // Particle
    public ParticleSystem explosionParticle;
    public ParticleSystem dirtParticle;

    // Sound effect (SFX)
    public AudioSource mainCameraAudio;
    public AudioSource playerAudio;
    public AudioClip jumpSound;
    public AudioClip crachSound;

    private float jumpForce = 10.0f;
    public float gravityModifier;
    public bool isOnGround = true; // 지면에서만 점프가 가능하도록
    public bool gameOver = false;

    // Start is called before the first frame update
    void Start()
    {
        playerRb = GetComponent<Rigidbody>();
        Physics.gravity = Physics.gravity * gravityModifier;

        playerAnim = GetComponent<Animator>();
        mainCameraAudio = Camera.main.GetComponent<AudioSource>();
        playerAudio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        /*
         * ForceMode
         * 1. 연속적인 힘 (예: 자동차 엑셀)
         * - Force (무게 적용)
         * - Acceleration (무게 무시)
         * 
         * 2. 순간적인 힘 (예: 뒤에서 누가 확 미는 것)
         * - Impulse (무게 적용)
         * - VelocityChange (무게 무시)
         */
        if (Input.GetKey(KeyCode.Space) && isOnGround && !gameOver)
        {
            playerRb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
            isOnGround = false;
            // 트리거를 발생시켜, 점프 애니메이션으로 애니메이션 스테이트를 변경한다.
            playerAnim.SetTrigger("Jump_trig");
            dirtParticle.Stop();
            playerAudio.PlayOneShot(jumpSound, 1.0f);
        }
    }

    // Player의 Collider/RigidBody가 다른 RigidBody/Collider와 접촉하기 시작하면 호출됨
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            isOnGround = true;
            dirtParticle.Play();
        }
        else if (collision.gameObject.CompareTag("Obstacle"))
        {
            Debug.Log("Game Over!");
            gameOver = true;
            playerAnim.SetBool("Death_b", true);
            playerAnim.SetInteger("DeathType_int", 1);
            explosionParticle.Play();
            dirtParticle.Stop();
            playerAudio.PlayOneShot(crachSound, 1.0f);
            mainCameraAudio.Stop();
        }
    }
}
