using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepeatBackground : MonoBehaviour
{
    private Vector3 startPos;
    private float halfWidth;

    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.position;
        halfWidth = GetComponent<BoxCollider>().size.x / 2; // Width값을 알아내기 위해, BoxCollider의 Size 프로퍼티를 사용한다.
    }

    // Update is called once per frame
    void Update()
    {
        // 배경화면 이미지는 같은 이미지 두장이 옆으로 붙어있는 형태이므로
        // 너비의 반만큼 이동한 순간에, 원래 위치로 보낸다.
        // x 값이 float 이기 때문에, equal (=)이 아닌, < 를 사용한다.
        if (transform.position.x < startPos.x - halfWidth)
        {
            transform.position = startPos;
        }
    }
}
