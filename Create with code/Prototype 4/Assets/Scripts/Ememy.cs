using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ememy : MonoBehaviour
{
    private Rigidbody enemyRb;
    private GameObject player;
    public float speed;

    // Start is called before the first frame update
    void Start()
    {
        enemyRb = GetComponent<Rigidbody>();
        player = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        // player�� �i�ƴٴϰ� �����.
        // Vector ���̳ʽ��� ����ؼ� ����� �Ÿ����� ���ϰ�, normalized�� ����ؼ� ���⸸ ���ܵд�. (�Ÿ��� ������� ���� ���� �����ϵ���)
        Vector3 lookDirection = (player.transform.position - transform.position).normalized;
        //Debug.Log(lookDirection);

        enemyRb.AddForce(lookDirection * speed);

        // ������ �������� Destroy �Ѵ�.
        if (transform.position.y < -10)
            Destroy(gameObject);
    }
}
