using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Rigidbody playerRb;
    private GameObject focalPoint;
    public GameObject powerupIndicator;
    public GameObject powerupMark;
    public float speed;
    public float powerupStrength = 15.0f;
    public bool hasPowerUp;

    // Start is called before the first frame update
    void Start()
    {
        playerRb = GetComponent<Rigidbody>();
        focalPoint = GameObject.Find("Focal Point");
        powerupMark = transform.Find("Powerup Mark").gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        float verticalInput = Input.GetAxis("Vertical");
        playerRb.AddForce(focalPoint.transform.forward * speed * verticalInput); // 나 자신의 z축이 아닌, focalPoint의 z축을 '앞'으로 설정.
        // Player 밑에 Powerup Indicator를 표시한다. Player가 굴러다니기 때문에, Player의 자식으로 둘 수 없다. (같이 굴러다님)
        powerupIndicator.transform.position = transform.position + new Vector3(0, -0.5f, 0);
    }

    // 두개가 부딪힐 경우 로직
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Powerup"))
        {
            hasPowerUp = true;
            Destroy(other.gameObject);
            StartCoroutine(PowerupCountdownRoutine());
            powerupIndicator.SetActive(true);
            powerupMark.SetActive(true);
        }
    }

    IEnumerator PowerupCountdownRoutine()
    {
        yield return new WaitForSeconds(7);
        hasPowerUp = false;
        powerupIndicator.SetActive(false);
        powerupMark.SetActive(false);
    }

    // 두개가 부딫힐 경우 로직 (물리 로직을 사용할 경우)
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy") && hasPowerUp)
        {
            Debug.Log("I have powerup");

            // Powerup을 가지고 있는 경우, powerupStrength 만큼 힘을 가해 적을 밀어낸다.
            Rigidbody enemyRb = collision.gameObject.GetComponent<Rigidbody>();
            Vector3 awayFromPlayer = collision.transform.position - transform.position;

            enemyRb.AddForce(awayFromPlayer * powerupStrength, ForceMode.Impulse);
        }
    }
}
