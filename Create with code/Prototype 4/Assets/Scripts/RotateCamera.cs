using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateCamera : MonoBehaviour
{
    public float rotationSpeed;
    public bool reverseHorizontalCamera = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float horizontalIput = Input.GetAxis("Horizontal");

        int isHorizontalCameraReverse = 1;
        if (reverseHorizontalCamera)
            isHorizontalCameraReverse = -1;

        transform.Rotate(isHorizontalCameraReverse * Vector3.up, horizontalIput * rotationSpeed * Time.deltaTime);
    }
}
